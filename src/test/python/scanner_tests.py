from scanner import Scanner, EOF
import unittest


class ScannerTestCase(unittest.TestCase):

    def test_scan(self):
        s = Scanner()
        s.set_data("asdf")
        self.assertEqual(0, s.bytes_to_read)
        self.assertEqual("a", s.next())
        self.assertEqual("s", s.next())
        s.rewind()
        self.assertEqual("a", s.next())
        self.assertEqual("a", s.get_lexeme())
        self.assertEqual("s", s.next())
        self.assertEqual("d", s.next())
        self.assertEqual("sd", s.get_lexeme())
        self.assertEqual("f", s.next())
        self.assertEqual(EOF, s.next())
        self.assertEqual(EOF, s.next())
        self.assertEqual(EOF, s.next())
        self.assertEqual("f", s.get_lexeme())
        foobar = "foobar"
        s.set_data(foobar)
        for i in range(len(foobar)):
            self.assertEqual(foobar[i], s.next())
        self.assertEqual("foobar", s.get_lexeme())

if __name__ == '__main__':
    unittest.main()
