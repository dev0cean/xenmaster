import unittest


class ParseStackTestCase(unittest.TestCase):

    def test_operations(self):
        stack = ParseStack()
        foo = ParseStack.Element(1, "foo")
        stack.push(foo)
        stack.push(2, "bar")
        self.assertEqual(2, len(stack))
        state, data = stack.pop()
        self.assertEqual(2, state)
        self.assertEqual("bar", data)
        self.assertIs(foo, stack.peek())
        self.assertEqual(1, len(stack))
        self.assertIs(foo, stack.pop())


if __name__ == '__main__':
    unittest.main()
