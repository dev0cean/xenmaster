#!/bin/bash
mv /etc/xenmaster/xensei.sql /etc/xenmaster/xensei.sql.last

mysqldump \
    --add-drop-database \
    --add-drop-table \
    --add-locks \
    --allow-keywords \
    --protocol=SOCKET \
    --delete-master-logs=FALSE \
    --compress=FALSE \
    --default-character-set=utf8 \
    --max_allowed_packet=1G \
    --host=localhost \
    --dump-date=TRUE \
    --force=FALSE \
    --single-transaction=TRUE \
    --user=xensei \
    --password=<PASSWORD> \
    --databases "xensei" > /etc/xenmaster/xensei.sql
