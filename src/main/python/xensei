#!/usr/bin/env python

import argparse
import collections
import os
import re
import sys

from ocsutil import ssh
from ocsutil.console import abort, timer
from ocsutil.process import exit_handlers, call


_cluster_dir = "cluster"
_host_dir = os.path.join(_cluster_dir, "host.d")

_platform_defaults = collections.OrderedDict({
    re.compile(r"[Rr][Hh][Ee][Ll]"): {
        "arch": "x86_64",
        "cores": 1,
        "mem": 1024,
        "disk": 4,
        "release": "5.7"
    },
    re.compile(r"[Ww][Ii][Nn]"): {
        "arch": "x86_64",
        "cores": 1,
        "mem": 1024,
        "disk": 4,
        "release": "2003-R2"
    },
    re.compile(""): {
        "arch": "x86_64",
        "cores": 1,
        "mem": 1024,
        "disk": 4,
        "release": ""
    }
})


class Command(object):
    """create a new virtual machine"""

    def __init__(self, args):
        super(Command, self).__init__()
        self.args = args

    @classmethod
    def configure_parser(cls, parser):
        raise NotImplementedError

    def execute(self):
        raise NotImplementedError


class CreateGuestTemplateCommand(Command):
    """create a new virtual machine template"""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument('name')
        parser.add_argument(
            "-o", "--os", default="rhel", choices=["rhel", "deb", "win"], help="what OS do you want")
        parser.add_argument(
            '-a', '--arch', default='x86_64', choices=['i386', 'x86_64'], help='guest CPU architecture')
        parser.add_argument(
            '-c', '--cores', metavar='CORE_COUNT', type=int, default=1, help='CPU cores')
        parser.add_argument(
            '-d', '--disk', dest='disk', metavar='DISK_CAPACITY', type=int, default=4,
                            help='disk capacity in gigabytes')
        parser.add_argument(
            '-i', '--ip', dest='ip', metavar='IP_ADDRESS', help='''
                An IPv4 address in dotted quad form. If specified, forward and reverse DNS records, and a static
                DHCP reservation will be added using the supplied IP address. An address suffix will define an address
                relative to the reverse-map network address corresponding to each authoritative forward zone matched by
                'name'.
            '''
        )
        parser.add_argument(
            '-m', '--mem', dest='mem', metavar='MEMORY_ALLOCATION', type=int, default=1024,
            help='memory capacity in megabytes'
        )
        parser.add_argument(
            '-r', '--roles', dest='roles', nargs='+', default=['default'], metavar='ROLE', help='roles'
        )
        parser.add_argument(
            '-s', '--swap', dest='swap', metavar='SWAP_CAPACITY', type=int,
            help='swap capacity in megabytes (default is autocalculated from memory allocation)'
        )
        parser.add_argument(
            '-z', '--zone', dest='zone', default='mgmt', choices=["mgmt"],
            help='network zone within the target environment'
        )


class CreateGuestCommand(Command):
    """create a new virtual machine"""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument(
            'name', metavar='hostname', help='''
                An fully or partially qualified hostname.
                The supplied name will be compared against all domains in
                the test realm until a matching environment name is found.
            '''
        )
        parser.add_argument(
            "-t", "--type", default="rhel", choices=["rhel", "deb", "win"], help="what OS do you want"
        )
        parser.add_argument(
            '-a', '--arch', dest='arch', default='x86_64', choices=['i386', 'x86_64'], help='guest CPU architecture'
        )
        parser.add_argument(
            '-c', '--cores', dest='cores', metavar='CORE_COUNT', type=int, default=1, help='CPU cores'
        )
        parser.add_argument(
            '-d', '--disk', dest='disk', metavar='DISK_CAPACITY', type=int, default=4, help='disk capacity in gigabytes'
        )
        parser.add_argument(
            '-i', '--ip', dest='ip', metavar='IP_ADDRESS', help='''
                An IPv4 address in dotted quad form. If specified, forward and reverse DNS records, and a static
                DHCP reservation will be added using the supplied IP address. An address suffix will define an address
                relative to the reverse-map network address corresponding to each authoritative forward zone matched by
                'name'.
            '''
        )
        parser.add_argument(
            '-m', '--mem', dest='mem', metavar='MEMORY_ALLOCATION', type=int, default=1024,
            help='memory capacity in megabytes'
        )
        parser.add_argument(
            '-r', '--roles', dest='roles', nargs='+', default=['default'], metavar='ROLE', help='roles'
        )
        parser.add_argument(
            '-s', '--swap', dest='swap', metavar='SWAP_CAPACITY', type=int,
            help='swap capacity in megabytes (default is autocalculated from memory allocation)'
        )
        parser.add_argument(
            '-z', '--zone', dest='zone', default='mgmt', choices=["mgmt"],
            help='network zone within the target environment'
        )


class StartGuestCommand(Command):
    """start an existing virtual machine"""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument('name', help='''The short name given to the guest when it was created.''')
        parser.add_argument('host', nargs='?', help='''The cluster host on which to start the guest. (optional)''')


class StopGuestCommand(Command):
    """stop a running virtual machine"""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument('name', help='''The short name given to the guest when it was created.''')


class DestroyGuestCommand(Command):
    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument('name', help='''The short name given to the guest when it was created.''')


class ListGuestsCommand(Command):
    """list all virtual machines currently running on the cluster"""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("scope", nargs='?',
                            help="Optional tenant or host name to which the search scope should be restricted.")

    def execute(self):
        with ClusterManager(args.cluster, master=args.master, config_file=args.config) as cluster:
            cluster.list_guests(args.scope)


class CreateNetworkCommand(Command):
    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument('name', help='''A short name to be used when displaying network information.''')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("cluster")
    parser.add_argument("-c", "--config")
    parser.add_argument("-m", "--master")
    parser.add_argument(
        "--log", default=sys.stdout, type=argparse.FileType('w'),
        help='the file to which diagnostic output should be written'
    )
    parser.add_argument("-v", dest="verbosity", action="count", default=0)
    parser.add_argument('--force', action='store_true', default=False, help='ahhhh ... push it')
    subparsers = parser.add_subparsers(title="commands")
    for command_name, command in {

        "create-guest": CreateGuestCommand,
        "start-guest": StartGuestCommand,
        "stop-guest": StopGuestCommand,
        #"restart-guest":  RestartGuestCommand,
        #"pause-guest":    PauseGuestCommand,
        #"resume-guest":   ResumeGuestCommand,
        #"migrate-guest":  MigrateGuestCommand,
        "destroy-guest": DestroyGuestCommand,
        "list-guests": ListGuestsCommand,

    }.iteritems():
        subparser = subparsers.add_parser(command_name, help=command.__doc__)
        subparser.set_defaults(command=command)
        command.configure_parser(subparser)

    args = parser.parse_args()
    with exit_handlers(abort, call(ssh.disconnect_all, debug=args.verbosity > 0)), timer():
        args.command(args).execute()
