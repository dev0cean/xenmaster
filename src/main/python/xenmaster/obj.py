from xenmaster.parser import Lexer, ParseToken, Parser

parse_terminals = ["EOF", "f", "v", "vn", "vt", "usemtl", "mtllib", "g"]
EOF = ParseToken("EOF", "f")
F = ParseToken("", "")
V = ParseToken("", "")
VN = ParseToken("", "")
VT = ParseToken("", "")
SLASH = ParseToken("", "")
INT = ParseToken("", "")
FLOAT = ParseToken("", "")
LABEL = ParseToken("", "")
USEMTL = ParseToken("USEMTL", "udemtl")
MTLLIB = ParseToken("MTLLIB", "mtllib")
G = ParseToken("G", "g")


class OBJLexer(Lexer):
    def __init__(self):
        super(OBJLexer, self).__init__()

    def _tokenize(self, string):
        import tokenize
        from cStringIO import StringIO

        type_map = {
            tokenize.NUMBER: "(literal)",
            tokenize.STRING: "(literal)",
            tokenize.OP: "(operator)",
            tokenize.NAME: "(name)",
        }
        for token_type, token_value in tokenize.generate_tokens(StringIO(string).next):
            try:
                yield type_map[token_type], token_value
            except KeyError:
                if token_type == tokenize.ENDMARKER:
                    break
                else:
                    raise SyntaxError("Syntax error")
        yield "(end)", "(end)"


class OBJParser(Parser):
    def __init__(self):
        super(OBJParser, self).__init__()
