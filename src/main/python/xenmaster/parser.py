from StringIO import StringIO
from collections import namedtuple, Sized

ParseTerminal = namedtuple("ParseTerminal", "type lexeme")
ParseToken = namedtuple("ParseToken", "type value")
ParseAction = namedtuple("ParseAction", "action index")
ParseProduction = namedtuple("ParseProduction", "non_terminal length reduction_function")

SHIFT = 1
REDUCE = 2
ACCEPT = 3


class Lexer(object):
    def __init__(self):
        super(Lexer, self).__init__()
        self._symbol_table = {}

    # def _tokenize(self, readline):
    #     for number, operator in token_pat.findall(program):
    #         if number:
    #             symbol = self._symbol_table["(literal)"]
    #             s = symbol()
    #             s.value = number
    #             yield s
    #         else:
    #             symbol = self._symbol_table.get(operator)
    #             if not symbol:
    #                 raise SyntaxError("Unknown operator")
    #             yield symbol()
    #     symbol = self._symbol_table["(end)"]
    #     yield symbol()

    def tokenize(self, readline):
        token = None
        c = ''
        state = 0
        startState = 0

    def _get_token(self, type_, lexeme):
        try:
            return self._symbol_table[lexeme]
        except KeyError:
            _token = ParseToken(type_, lexeme)
            self._symbol_table[lexeme] = _token
            return _token


class ParseStack(Sized):
    Element = namedtuple("Element", "state data")
    Element.__repr__ = lambda self: "(%4i) %s" % (self.state, self.data.__repr__())

    def __init__(self):
        super(ParseStack, self).__init__()
        self.elements = []

    def push(self, *args):
        try:
            self.elements.append(self.Element(*args))
        except TypeError:
            self.elements.append(*args)

    def pop(self):
        return self.elements.pop()

    def pops(self, count):
        buf = self.elements[:count + 1:-1]
        del self.elements[-count:]
        return buf

    def peek(self):
        return self.elements[len(self.elements) - 1] if self.elements else None

    def __len__(self):
        return len(self.elements)

    def __repr__(self):
        return '\n'.join(e.__repr__() for e in reversed(self.elements))


class Parser(object):
    def __init__(self):
        super(Parser, self).__init__()
        self.lexer = None

    def parse(self, string):
        _next = self.lexer.tokenize(StringIO(string).next).next
        token = _next()
