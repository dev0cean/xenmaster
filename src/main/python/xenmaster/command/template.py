import argparse
import os
import textwrap
from ocsutil import AttributedDict, si_units
from ocsutil.console import display_table, DisplayField, ANSI, info, warn, Command

from libxm.exceptions import XenmasterError
from libxm.model import transaction
from libxm.model.network import GuestInterfaceDriver
from libxm.model.storage import BlockDeviceSpec, BlockDeviceType
from libxm.service.guest_service import derive_guest_template, create_guest_template
from libxm.service.host_service import get_cluster_with_name, get_host_with_name
from libxm.service.guest_service import parse_guest_template_name, get_available_oss, modify_guest_template, \
    delete_guest_template, parse_guest_template_name_hard, parse_tenant_name, get_guest_templates
from libxm.service.network_service import delete_template_interface, bind_template_interface, create_template_interface
from libxm.service.storage_service import get_disk_image_with_name, delete_virtual_block_device, delete_disk_image,\
    copy_disk_image, get_storage_repositories_for_tenant, insert_disk_image, eject_disk_image, import_disk_image, \
    rename_virtual_block_device, set_guest_boot_device, get_storage_repository_with_name, create_disk_image, \
    create_virtual_block_device, resize_virtual_block_device


guest_defaults = {
    "hvm": False,
    "arch": "x86_64",
    "vcpus": 1,
    "memory": 1024,
    "os_name": "RHEL",
    "vnc": True,
    "vnc_password": "d3v734m",
}
os_defaults = {
    "Debian": {
        "os_release": "wheezy"
    },
    "RHEL": {
        "os_release": "5.7"
    },
    "Windows": {
        "os_release": "2003-R2"
    },
    "BIG-IP": {
        "os_release": "10.2.2"
    },
}
aos = None


def available_os():
    global aos
    if aos is None:
        aos = get_available_oss()
    return aos


def fill_defaults(opts):
    for key, default_value in guest_defaults.iteritems():
        opts.setdefault(key, default_value)
    opts.setdefault("os_release", os_defaults.get(opts["os_name"], {}).get("os_release"))
    return opts


def _display_vm_info(template):
    class vm_attr(object):

        def __init__(self, label, value, option=None):
            super(vm_attr, self).__init__()
            self.label = label
            self.value = value
            self.option = option

    display_table(
        [
            vm_attr("Tenant", template.tenant.shortname),
            vm_attr("Name", template.name),
            vm_attr("VMM Model", template.vmm.model),
            vm_attr("Device Model", template.device_model),
            vm_attr("Target OS", str(template.os).replace('_', ' ')),
            vm_attr("Architecture", template.arch),
            vm_attr("CPU Cores", template.vcpus),
            vm_attr("Memory", si_units(template.memory)),
        ], [
            DisplayField("Property", lambda a: a.label),
            DisplayField("Value", lambda a: a.value),
        ], group_by=DisplayField("Basic VM Parameters", None), group_headings=True,
    )
    display_table(
        [
            vm_attr("CPU Mapping", template.cpus or '', "cpus"),
            vm_attr("CPU Weight", template.cpu_weight or '', "cpu_weight"),
            vm_attr("CPU Cap", template.cpu_cap or '', "cpu_cap"),
            vm_attr("Max Memory", template.maxmem or '', "maxmem"),
            vm_attr("Shadow Memory", template.shadow_memory or '', "shadow_memory"),
            vm_attr("Serial", template.serial or '', "serial"),
            vm_attr("Console", template.console or '', "console"),
            vm_attr("Loader", template.loader or '', "loader"),
            vm_attr("Bootloader Arguments", template.bootloader_args or '', "bootloader_args"),
            vm_attr("Kernel Extra Arguments", template.kernel_extra_args or '', "kernel_extra_args"),
            vm_attr("Enable Hardware Assisted Paging", template.hap, "hap"),
            vm_attr("Suppress Spurious Page Faults", template.suppress_spurious_page_faults,
                    "suppress_spurious_page_faults"),
            vm_attr("Enable Xen Platform PCI", template.enable_xen_platform_pci, "enable_xen_platform_pci"),
            vm_attr("Enable ACPI", template.enable_acpi, "enable_acpi"),
            vm_attr("Enable APIC", template.enable_apic, "enable_apic"),
            vm_attr("Enable PAE", template.enable_pae, "enable_pae"),
            vm_attr("Enable HPET", template.enable_hpet, "enable_hpet"),
            vm_attr("Enable MS Virtualization Support", template.enable_viridian, "enable_viridian"),
            vm_attr("Enable Nested HVM", template.enable_nested_hvm, "enable_nested_hvm"),
            vm_attr("Enable Graphics", template.enable_graphics, "enable_graphics"),
            vm_attr("Enable SDL", template.enable_sdl, "enable_sdl"),
            vm_attr("Enable OpenGL", template.enable_opengl, "enable_opengl"),
            vm_attr("Use Standard VGA Emulation", template.stdvga, "stdvga"),
            vm_attr("Video Memory", template.video_memory or '', "video_memory"),
            vm_attr("Sound Hardware", template.sound_hw or '', "sound_hw"),
            vm_attr("Power-off Action", template.on_poweroff or '', "on_poweroff"),
            vm_attr("Reboot Action", template.on_reboot or '', "on_reboot"),
            vm_attr("Crash Action", template.on_crash or '', "on_crash"),
        ], [
            DisplayField("Property", lambda a: a.label),
            DisplayField("Value", lambda a: a.value),
            DisplayField("Advanced Option Key", lambda a: a.option),
        ], group_by=DisplayField("Advanced VM Parameters", None), group_headings=True,
    )
    display_table(
        [
            vm_attr("Enable VNC", template.vnc, "vnc"),
            vm_attr("Auto Port Allocation", template.vnc_auto_port, "vnc_auto_port"),
            vm_attr("VNC Server Bind Address", template.vnc_listen or '', "vnc_listen"),
            vm_attr("Password", template.vnc_password or '', "vnc_password"),
            vm_attr("Console", template.vnc_console, "vnc_console"),
            vm_attr("Display Index", template.vnc_display or '', "vnc_display"),
        ], [
            DisplayField("Property", lambda a: a.label),
            DisplayField("Value", lambda a: a.value),
            DisplayField("Advanced Option Key", lambda a: a.option),
        ], group_by=DisplayField("Network Console Parameters", None), group_headings=True,
    )


class CreateTemplate(Command):
    __doc__ = textwrap.dedent("""\
    Create a new guest template, by supplying attribute values,
    or by customising an existing template or an existing guest.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="A tenant-qualified name for the template (template.tenant).")
        parser.add_argument('base', nargs="?", default=argparse.SUPPRESS,
                            help="The name of an existing template or guest to use as a base.")
        parser.add_argument(
            "-H", dest="hvm", action="store_true", default=argparse.SUPPRESS,
            help="Use a virtualised hardware (HVM) VMM model.\n"
                 "(default: para-virtualised guest-kernel-supported (PVM))"
        )
        parser.add_argument(
            '-a', dest='arch', default=argparse.SUPPRESS, choices=['i386', 'x86_64'],
            help='CPU architecture. (default: %s)' % guest_defaults['arch']
        )
        parser.add_argument(
            '-c', dest='vcpus', type=int, default=argparse.SUPPRESS, choices=range(1, 5),
            help='Virtual CPU core count. (default: %i)' % guest_defaults['vcpus']
        )
        parser.add_argument(
            '-m', dest='memory', metavar='MEMORY', type=int, default=argparse.SUPPRESS,
            help='Memory capacity in megabytes. (default: %i)' % guest_defaults['memory']
        )
        parser.add_argument(
            '-d', dest="disks", metavar="device:type:mode:size", action="append", default=argparse.SUPPRESS,
            help=textwrap.dedent("""\
            Specification for a hard disk. Use multiple times to configure multiple devices.
            Where the components have the following meanings:
            device: The name by which the virtual block device will be known to the
                     guest (e.g. xvda, sdb, hdc).
            type:   The block device type.
                     {%s}
            mode:   The access mode of the device. {r|w}
            size:   The desired disk capacity in SI megabytes.""" % '|'.join(BlockDeviceType.values))
        )
        parser.add_argument(
            '-n', dest="nics", metavar="type", action="append", default=argparse.SUPPRESS,
            help=textwrap.dedent("""\
            Add a network interface of the specified emulated hardware type.
            Use multiple times to configure multiple devices.
            Supported nic types are: {%s}
            (default: netfront)""" % '|'.join(GuestInterfaceDriver.values))
        )
        parser.add_argument(
            "-o", dest="os_name", choices=sorted(set([o.name for o in available_os()])),
            default=argparse.SUPPRESS,
            help="Target OS. Used to select sane VMM defaults. (default: %s)" % guest_defaults['os_name']
        )
        parser.add_argument(
            "-r", dest="os_release", default=argparse.SUPPRESS,
            help="OS release. e.g. 5.7 or 2003-R2. (default: %s)" %
                 os_defaults.get(guest_defaults["os_name"])["os_release"]
        )
        parser.add_argument(
            "-p", dest="vnc_password", metavar="PASSWORD", default=argparse.SUPPRESS,
            help="Password for console access over VNC. (default: %s)" % guest_defaults["vnc_password"]
        )
        advanced = parser.add_argument_group("key-value arguments", description="advanced options as key=value pairs")
        advanced.add_argument("advanced", nargs=argparse.ZERO_OR_MORE, metavar="key=value",
                              help="A key=value tuple which will be passed through to the guest service via **kwargs.")

        parser.epilog = "For example:\n" \
                        "\txm template create -m 8192 " \
                        "-d xvda:hdd:w:51200 -d xvdb:hdd:w:8192 app1 rhel57"

    def execute(self, name, disks=None, nics=None, base=None, advanced=None, **kwargs):
        def device_spec(device_string):
            try:
                device, device_type, mode, size = device_string.split(':', 3)
                return BlockDeviceSpec(device, device_type, (mode == 'r'), int(size) if size else None)
            except:
                raise XenmasterError(
                    "Invalid disk device specification: %s" % device_string,
                    textwrap.dedent("""\
                    Disk device specifications must be of the form device:type:mode:size, where
                    component attributes have the following meanings:

                    device: The name by which the virtual block device will be known to the
                             guest (e.g. xvda, sdb, hdc).
                    type:   The block device type.
                             {%s}
                    mode:   The access mode of the device. {r|w}
                    size:   The desired disk capacity in SI megabytes.""" % '|'.join(BlockDeviceType.values)))

        tenant_name, _, template_name = name.rpartition(':')
        tenant = parse_tenant_name(tenant_name)
        if base and '=' in base:
            # argparse misinterpretted the first advanced tuple as a base
            key, value = base.split('=', 1)
            if value:
                kwargs[key] = value
            base = None
        if advanced:
            for tup in advanced:
                key, value = tup.split('=', 1)
                if value:
                    kwargs[key] = value
        block_devices = (device_spec(disk) for disk in disks) if disks else None
        if base:
            derive_guest_template(template_name, tenant, base, block_devices, nics, **kwargs)
        else:
            fill_defaults(kwargs)
            create_guest_template(template_name, tenant, block_devices=block_devices, nics=nics, **kwargs)
        info("A new template '%s' has been created with the following configuration:" % name)
        TemplateDetail().execute(name)


class DeleteTemplate(Command):
    __doc__ = textwrap.dedent("""\
    Delete an existing guest template, including all device
    definitions and any template disk images. Shared disk
    images will not be deleted.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="The tenant-qualified name of the template to delete (template.tenant)")

    def execute(self, name, **kwargs):
        template, _ = parse_guest_template_name_hard(name)
        delete_guest_template(template)


class ModifyTemplate(Command):
    __doc__ = textwrap.dedent("""\
    Modify VM properties of an existing guest template, by
    supplying new values for attributes to be updated.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument('name', default=argparse.SUPPRESS, help='''
            The tenant-qualified name of the template to modify.
        ''')
        parser.add_argument(
            "-H", dest="hvm", action="store_true", default=argparse.SUPPRESS,
            help="Use a virtualised hardware (HVM) VMM model."
        )
        parser.add_argument(
            '-a', dest='arch', default=argparse.SUPPRESS, choices=['i386', 'x86_64'],
            help='CPU architecture.'
        )
        parser.add_argument(
            '-c', dest='vcpus', type=int, default=argparse.SUPPRESS, choices=range(1, 5),
            help='Virtual CPU core count.'
        )
        parser.add_argument(
            '-m', dest='memory', metavar='MEMORY', type=int, default=argparse.SUPPRESS,
            help='Memory capacity in megabytes.'
        )
        parser.add_argument(
            "-o", dest="os_name", choices=sorted(set([o.name for o in available_os()])),
            default=argparse.SUPPRESS,
            help="Target OS. Used to select sane VMM defaults."
        )
        parser.add_argument(
            "-r", dest="os_release", default=argparse.SUPPRESS,
            help="OS release. e.g. 5.7 or 2003-R2."
        )
        parser.add_argument(
            "-p", dest="vnc_password", metavar="PASSWORD", default=argparse.SUPPRESS,
            help="Password for console access over VNC."
        )
        advanced = parser.add_argument_group("key-value arguments", description="advanced options as key=value pairs")
        advanced.add_argument("advanced", nargs=argparse.ZERO_OR_MORE, metavar="key=value",
                              help="A key=value tuple which will be passed through to the guest service via **kwargs.")

    def execute(self, name, advanced=None, **kwargs):
        if advanced:
            for tup in advanced:
                key, value = tup.split('=', 1)
                if value:
                    kwargs[key] = value
        template = modify_guest_template(name, **kwargs)
        info("Template %s has been modified. The updated VM configuration is as follows:" % template.qualified_name)
        _display_vm_info(template)


class TemplateDetail(Command):
    __doc__ = textwrap.dedent("""\
    Display details of a template's VM spec and any defined
    block devices, network interfaces and disk images.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="The tenant-qualified name of the template (template.tenant)")

    def execute(self, name, **kwargs):
        template, _ = parse_guest_template_name_hard(name)
        _display_vm_info(template)
        _display_block_device_info(template)
        _display_network_device_info(template)


class ListTemplates(Command):
    """Display a summary of defined guest templates."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("tenant", nargs=argparse.OPTIONAL,
                            help="Optional Tenant to which the listing will be constrained.")

    def execute(self, tenant=None, **kwargs):
        if tenant:
            tenant = parse_tenant_name(tenant)
        templates = get_guest_templates(tenant)
        display_table(
            templates, [
                DisplayField("Name", lambda t: t.name),
                DisplayField("VMM model", lambda t: t.vmm.model),
                DisplayField("Device model", lambda t: t.device_model),
                DisplayField("OS", lambda t: str(t.os).replace('_', ' ')),
                DisplayField("Arch", lambda t: t.arch),
                DisplayField("Cores", lambda t: t.vcpus, justify='r'),
                DisplayField("Memory (MB)", lambda t: t.memory, justify='r'),
            ],
            group_by=DisplayField(lambda t: "%s templates" % t, lambda t: t.tenant.shortname),
            group_headings=True,
            order_by=lambda t: t.name
        )


########################################################################################################################
# Block Devices
########################################################################################################################

def _display_block_device_info(template):
    display_table(
        template.block_devices.values(), [
            DisplayField("Name", lambda d: d.name),
            DisplayField("Type", lambda d: d.device_type),
            DisplayField("Mode", lambda d: "r" if d.read_only else "w"),
            DisplayField("Image", lambda d: d.disk_image.qualified_name if d.disk_image else ''),
            DisplayField("Boot", lambda d: 'X' if d is d.guest.boot_device else ''),
            # DisplayField("Driver", lambda d: d.driver),
            DisplayField("Capacity", lambda d: si_units(d.disk_image.virtual_size if d.disk_image else
                                                        d.capacity if d.capacity else 0), justify='r'),
        ], group_by=DisplayField("Block Devices", None), group_headings=True,
        order_by=lambda d: d.name
    )


class AddNewHardDisk(Command):
    __doc__ = textwrap.dedent("""\
    Add a virtual hard-disk block device to the named template,
    optionally allocating a backing disk image within a storage
    repository.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-c", "--create", action="store_true", default=False,
                            help="Allocate a backing disk image for the block device.")
        parser.add_argument("-r", "--repo",
                            help=textwrap.dedent("""\
                            The tenant accessible repository in which to create the new disk image.
                            If unspecified the tenant's default repository will be used.
                            """))
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("device", help="The name by which the virtual block device will be known to guests.")
        parser.add_argument("capacity", type=int, help="The disk image size in megabytes (1024^2 bytes).")
        parser.add_argument("name", nargs=argparse.OPTIONAL,
                            help=textwrap.dedent("""\
                            A name for the created disk image. If not supplied, a name will be generated from
                            the template name and device.
                            """))

    def execute(self, template, device, capacity, name=None, create=False, repo=None, **kwargs):
        _template, tenant = parse_guest_template_name_hard(template)
        repository = get_storage_repository_with_name(repo, tenant) \
            if repo else tenant.default_storage_repository
        image_name = name if name else "%s-%s" % (_template.safe_qualified_name, device)
        with transaction():
            disk_image = None
            if create:
                disk_image = create_disk_image(image_name, repository, capacity, shareable=False, read_only=False)
            create_virtual_block_device(
                _template, BlockDeviceSpec(
                    device, BlockDeviceType.hdd, False,
                    disk_image.virtual_size if disk_image else capacity), disk_image)
        info("Template '%s' has been modified. The updated block device configuration is as follows:"
             % _template.qualified_name)
        _display_block_device_info(_template)


class ResizeHardDisk(Command):
    __doc__ = textwrap.dedent("""\
    Alter the size of a hard disk defined upon a guest template.
    """)

    @classmethod
    def configure_parser(cls, parser):
        """
        :type parser: argparse.ArgumentParser
        """
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("device", help="The name of the virtual block device to resize.")
        parser.add_argument("new_size", help="The new size for the virtual hard disk (in MB).")

    def execute(self, template, device, new_size, **kwargs):
        _template, tenant = parse_guest_template_name_hard(template)
        block_device = _template.block_devices.get(device)
        if block_device is None:
            raise XenmasterError("Template '%s' has no block device '%s'." % _template.qualified_name, device)
        resize_virtual_block_device(block_device, new_size)
        info("Template '%s' has been modified. The updated block device configuration is as follows:"
             % _template.qualified_name)
        _display_block_device_info(_template)


class AddExistingHardDisk(Command):
    __doc__ = textwrap.dedent("""\
    Associate an existing disk image with a template as a new virtual
    block device.
    """)

    @classmethod
    def configure_parser(cls, parser):
        """
        :type parser: argparse.ArgumentParser
        """
        parser.add_argument("-m", "--mode", choices=['r', 'w'], default='w',
                            help=textwrap.dedent("""\
                            The access mode of the virtual block device. This is ignored if the disk will be shared
                            and the existing image's access mode is read-only.
                            """))
        copy = parser.add_argument_group()
        copy.add_argument("-c", "--copy", action="store_true", default=False,
                          help=textwrap.dedent("""\
                          Make a separate copy of the virtual disk image. The default behaviour is to share the
                          existing disk image read-only.
                          """))
        copy.add_argument("name", nargs=argparse.OPTIONAL,
                          help=textwrap.dedent("""\
                          Optional name for a copied disk image. If 'name' is qualified with the name of a
                          storage repository accessible to the tenant, the copy will be created in the named
                          storage repository. If 'name' is unqualified the tenant's default repository will be
                          used. If a copy is required and 'name' is not supplied, a name will be generated from
                          the template name and device.
                          """))
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("device", help="The name by which the virtual block device will be known to guests.")
        parser.add_argument("image", help="The repository-qualified disk image name (repository/image).")

    def execute(self, template, device, image, mode, copy=False, name=None, **kwargs):
        _template, tenant = parse_guest_template_name_hard(template)
        repo_name, _, image_name = image.rpartition('/')
        if repo_name:
            repository = get_storage_repository_with_name(repo_name, tenant)
        else:
            repository = tenant.default_storage_repository
        disk_image = get_disk_image_with_name(image_name, repository)
        if disk_image is None:
            raise XenmasterError("No disk image '%s' could be found in '%s'" % (image_name, repository.name))
        with transaction():
            if copy:
                # define the parameters for an image copy
                dest_repo = tenant.default_storage_repository
                read_only = (mode == 'r')
                if name:
                    repo_name, _, image_name = name.rpartition('/')
                    if repo_name:
                        dest_repo = get_storage_repository_with_name(repo_name, tenant)
                else:
                    image_name = "%s-%s" % (_template.safe_qualified_name, device)
                disk_image = copy_disk_image(disk_image, image_name, dest_repo, read_only)
            else:
                read_only = (mode == 'r') or disk_image.read_only

            spec = BlockDeviceSpec(device, BlockDeviceType.hdd, read_only, disk_image.virtual_size)
            create_virtual_block_device(_template, spec, disk_image)
        info("Template '%s' has been modified. The updated block device configuration is as follows:"
             % _template.qualified_name)
        _display_block_device_info(_template)


class ImportExistingHardDisk(Command):
    __doc__ = textwrap.dedent("""\
    Import an existing physical block device as a disk image,
    and associate it with a template as a new virtual block device.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-m", "--mode", choices=['r', 'w'], default='r',
                            help="The access mode of the disk image. (default: r)")
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("device", help="The name by which the virtual block device will be known to guests.")
        parser.add_argument("path", help="The full path (host.cluster:path) to the existing physical block device.")
        parser.add_argument("name", nargs=argparse.OPTIONAL,
                            help=textwrap.dedent("""\
                            If specified, a copy of 'path' will be created with the supplied 'name'.
                            If 'name' is qualified with the name of a storage repository, the copy will be
                            created in the named storage repository. If 'name' is unqualified the tenant's
                            default storage repository will be used.
                            If a copy is required, because 'path' lies outside any tenant storage repository,
                            and 'name' is not supplied, the image will be created in the tenant's default
                            storage repository with a name generated from the template name and device.
                            """))

    def execute(self, template, device, path, mode, name=None, **kwargs):
        _template, tenant = parse_guest_template_name_hard(template)
        host, _, _path = path.partition(':')
        if host is '' or _path is '':
            raise XenmasterError("Unable to import '%s'" % path,
                                 "The supplied path was not of the form host.cluster:path.")
        host_name, _, cluster_name = host.partition('.')
        if not cluster_name:
            raise XenmasterError("Path host was not qualified with a cluster name.")
        cluster = get_cluster_with_name(cluster_name)
        cluster_node = get_host_with_name(host_name, cluster)
        read_only = (mode == 'r')
        dest_repo = tenant.default_storage_repository
        image_name = "%s-%s" % (_template.safe_qualified_name, device)
        if name:
            repo_name, _, image_name = name.rpartition('/')
            if repo_name:
                dest_repo = get_storage_repository_with_name(repo_name, tenant)
        else:
            candidate_repos = get_storage_repositories_for_tenant(tenant)
            for repo in candidate_repos:
                if _path.startswith(os.path.join(str(repo.path), '')):
                    dest_repo = repo
                    image_name = os.path.relpath(_path, str(repo.path))
                    break
        with transaction():
            disk_image = import_disk_image(cluster_node, _path, image_name, dest_repo, read_only=read_only)
            spec = BlockDeviceSpec(device, BlockDeviceType.hdd, read_only, disk_image.virtual_size)
            create_virtual_block_device(_template, spec, disk_image)
        info("Template '%s' has been modified. The updated block device configuration is as follows:"
             % _template.qualified_name)
        _display_block_device_info(_template)


class AddOpticalDrive(Command):
    """Add a virtual optical block device to a template."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("device", help="The name by which the virtual block device will be known to guests.")

    def execute(self, template, device, **kwargs):
        _template, tenant = parse_guest_template_name_hard(template)
        create_virtual_block_device(_template, BlockDeviceSpec(device, BlockDeviceType.cdrom, True, None))
        info("Template '%s' has been modified. The updated block device configuration is as follows:"
             % _template.qualified_name)
        _display_block_device_info(_template)


class RenameBlockDevice(Command):
    """Rename a template's virtual block device."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("device", help="The current device name.")
        parser.add_argument("new_name", help="The new device name.")

    def execute(self, template, device, new_name, **kwargs):
        _template, _ = parse_guest_template_name_hard(template)
        block_device = _template.block_devices.get(device)
        if block_device is None:
            raise XenmasterError("Template '%s' has no block device '%s'." % _template.qualified_name, device)
        rename_virtual_block_device(block_device, new_name)
        info("Template %s has been modified. The updated block device configuration is as follows:"
             % _template.qualified_name)
        _display_block_device_info(_template)


class RemoveBlockDevice(Command):
    __doc__ = textwrap.dedent("""\
    Remove a template's virtual block device.
    Any associated disk image will NOT be deleted.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("device", help="The name of the virtual block device to remove.")

    def execute(self, template, device, **kwargs):
        _template, _ = parse_guest_template_name_hard(template)
        block_device = _template.block_devices.get(device)
        if block_device is None:
            raise XenmasterError("Template '%s' has no block device '%s'." % _template.qualified_name, device)
        delete_virtual_block_device(block_device)
        info("Template '%s' has been modified. The updated block device configuration is as follows:"
             % _template.qualified_name)
        _display_block_device_info(_template)


class DeleteBlockDevice(Command):
    """Delete a template's virtual block device and its backing disk image."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("device", help="The name of the virtual block device to delete.")

    def execute(self, template, device, **kwargs):
        _template, _ = parse_guest_template_name_hard(template)
        block_device = _template.block_devices.get(device)
        if block_device is None:
            raise XenmasterError("Template '%s' has no block device '%s'." % (_template.qualified_name, device))
        image = block_device.disk_image
        delete_virtual_block_device(block_device)
        if len(image.block_devices) > 1:
            warn("Not deleting shared disk image %s." % image.qualified_name)
            return
        delete_disk_image(image)
        info("Template '%s' has been modified. The updated block device configuration is as follows:"
             % _template.qualified_name)
        _display_block_device_info(_template)


class SetBootDevice(Command):
    """Set a template's boot device."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("device", help="The template device name.")

    def execute(self, template, device, **kwargs):
        _template, _ = parse_guest_template_name_hard(template)
        set_guest_boot_device(_template, device)
        info("Template '%s' has been modified. The updated block device configuration is as follows:"
             % _template.qualified_name)
        _display_block_device_info(_template)


class InsertRemovableMedia(Command):
    """'Insert' a removable media disk image into the named device."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("device", help="The guest device name")
        parser.add_argument("image", help="The repository-qualified disk image name (repository/image).")

    def execute(self, template, device, image, **kwargs):
        _template, tenant = parse_guest_template_name_hard(template)
        dev = _template.block_devices.get(device)
        if dev is None:
            raise XenmasterError("'%s' has no device '%s'" % (_template.qualified_name, device))
        repo_name, _, image_name = image.rpartition('/')
        if repo_name:
            repo = get_storage_repository_with_name(repo_name, tenant)
        else:
            repo = tenant.default_storage_repository
        media = get_disk_image_with_name(image_name, repo)
        if media is None:
            raise XenmasterError("No disk image '%s' could be found in '%s'" % (image_name, repo.name))
        insert_disk_image(dev, media)
        info("Template '%s' has been modified. The updated block device configuration is as follows:"
             % _template.qualified_name)
        _display_block_device_info(_template)


class EjectRemovableMedia(Command):
    """'Eject' a removable media disk image from the named device."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("device", help="The guest device name")

    def execute(self, template, device, **kwargs):
        _template, tenant = parse_guest_template_name_hard(template)
        dev = _template.block_devices.get(device)
        if dev is None:
            raise XenmasterError("'%s' has no device '%s'" % (_template.qualified_name, device))
        eject_disk_image(dev)
        info("Template '%s' has been modified. The updated block device configuration is as follows:"
             % _template.qualified_name)
        _display_block_device_info(_template)


class ListBlockDevices(Command):
    """List a template's virtual block devices."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")

    def execute(self, template, **kwargs):
        _template, _ = parse_guest_template_name(template)
        _display_block_device_info(_template)


########################################################################################################################
# Network Devices
########################################################################################################################

def _display_network_device_info(template):
    display_table(
        template.network_interfaces, [
            DisplayField("Index", lambda d: d.index),
            DisplayField("Driver", lambda d: d.driver),
            DisplayField("Network", lambda d: d.network.qualified_name if d.network else ''),
            DisplayField("VLANs", lambda d: ', '.join(v.name for v in sorted(d.vlans))),
            DisplayField("Rate Limit", lambda d: d.rate),
        ], group_by=DisplayField("Network Interfaces", None), group_headings=True,
        order_by=lambda d: d.index
    )


class AddNetworkInterface(Command):
    """Create a new template network interface."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-d", dest="driver", choices=GuestInterfaceDriver.values,
                            help="The hardware emulation model to present to guests. (default: %s)"
                                 % GuestInterfaceDriver.values.netfront)
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("network", nargs='?',
                            help="Optionally configure the device for connection to a specific network.")
        parser.add_argument("vlans", metavar="vlan", nargs="*",
                            help=textwrap.dedent("""\
                            An optional, space-separated list of names of existing
                            VLANs available on the named network.
                            """))

    def execute(self, template, network, driver=None, vlans=None):
        create_template_interface(template, driver, network, vlans)
        info("Template '%s' has been modified. The updated network device configuration is as follows:" % template)
        _template, _ = parse_guest_template_name_hard(template)
        _display_network_device_info(_template)


class BindNetworkInterface(Command):
    """Associate a template network interface with a network, optionally specifying one or more VLANs."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("index", type=int, choices=range(6),
                            help="The index of the target among the guest's list of interfaces.")
        parser.add_argument("network", help="The switch network to which the nic will connect. e.g. test.trunk")
        parser.add_argument("vlans", metavar="vlan", nargs="*",
                            help="A list (possibly empty) of names of existing VLANs available on the named network.")

    def execute(self, template, index, network, vlans=None):
        bind_template_interface(template, index, network, vlans)
        info("Template '%s' has been modified. The updated network device configuration is as follows:" % template)
        _template, _ = parse_guest_template_name_hard(template)
        _display_network_device_info(_template)


class UnbindNetworkInterface(Command):
    """Dissociate a template network interface from any network."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("index", type=int, choices=range(6),
                            help="The index of the target among the guest's list of interfaces.")

    def execute(self, template, index):
        bind_template_interface(template, index, None)
        info("Template '%s' has been modified. The updated network device configuration is as follows:" % template)
        _template, _ = parse_guest_template_name_hard(template)
        _display_network_device_info(_template)


class RemoveNetworkInterface(Command):
    """Remove a network interface from a guest."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")
        parser.add_argument("index", type=int, choices=range(6),
                            help="The index of the target among the guest's list of interfaces.")

    def execute(self, template, index):
        delete_template_interface(template, index)
        info("Template '%s' has been modified. The updated network device configuration is as follows:" % template)
        _template, _ = parse_guest_template_name_hard(template)
        _display_network_device_info(_template)


class ListNetworkInterfaces(Command):
    """List a guest's network interfaces."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("template", help="The tenant-qualified name (template.tenant) of the template.")

    def execute(self, template, **kwargs):
        _template, _ = parse_guest_template_name_hard(template)
        _display_network_device_info(_template)


meta = AttributedDict({
    "help": __doc__,
    "actions": [
        (ANSI.BOLD_ON + "Basic actions" + ANSI.RESET, [
            ("create", CreateTemplate),
            ("delete", DeleteTemplate),
            ("list", ListTemplates),
            ("modify", ModifyTemplate),
            ("detail", TemplateDetail),
        ]),
        (ANSI.BOLD_ON + "Block device actions" + ANSI.RESET, [
            ("add-hdd", AddNewHardDisk),
            ("bind-hdd", AddExistingHardDisk),
            ("import-hdd", ImportExistingHardDisk),
            ("resize-hdd", ResizeHardDisk),
            ("add-cdrom", AddOpticalDrive),
            ("insert-media", InsertRemovableMedia),
            ("eject-media", EjectRemovableMedia),
            ("set-boot-device", SetBootDevice),
            ("rename-block-device", RenameBlockDevice),
            ("remove-block-device", RemoveBlockDevice),
            ("delete-block-device", DeleteBlockDevice),
            ("list-block-devices", ListBlockDevices),
        ]),
        (ANSI.BOLD_ON + "Network interface actions" + ANSI.RESET, [
            ("add-nic", AddNetworkInterface),
            ("bind-nic", BindNetworkInterface),
            ("unbind-nic", UnbindNetworkInterface),
            ("remove-nic", RemoveNetworkInterface),
            ("list-nics", ListNetworkInterfaces),
        ]),
    ]
})
