import argparse
import os
import textwrap
from ocsutil import AttributedDict, si_units
from ocsutil.console import Command, info, display_table, ANSI, DisplayField

from libxm.exceptions import XenmasterError
from libxm.model import transaction
from libxm.model.network import GuestInterfaceDriver
from libxm.model.storage import BlockDeviceSpec, BlockDeviceType
from libxm.service.guest_service import get_guest_with_name, \
    delete_guest, create_guest, start_guest, stop_guest, create_disk_image, create_virtual_block_device, get_tenants, \
    connect_to_guest_console, parse_guest_template_name, get_available_oss, parse_guest_name_hard, modify_guest, \
    parse_tenant_name
from libxm.service.host_service import get_cluster_with_name, sync_cluster_state, get_host_with_name, get_clusters, \
    sync_guest_state
from libxm.service.network_service import bind_guest_interface, delete_guest_interface, parse_network_name_hard, \
    get_vlan_with_name, create_guest_interface
from libxm.service.storage_service import get_disk_image_with_name, delete_virtual_block_device, delete_disk_image,\
    copy_disk_image, get_storage_repositories_for_tenant, insert_disk_image, connect_virtual_block_device, \
    eject_disk_image, disconnect_virtual_block_device, rename_virtual_block_device, get_storage_repository_with_name, \
    set_guest_boot_device, import_disk_image


guest_defaults = {
    "hvm": False,
    "arch": "x86_64",
    "vcpus": 1,
    "memory": 1024,
    "os_name": "RHEL",
    "vnc": True,
    "vnc_password": "d3v734m",
}
os_defaults = {
    "Debian": {
        "os_release": "wheezy"
    },
    "RHEL": {
        "os_release": "5.7"
    },
    "Windows": {
        "os_release": "2003-R2"
    },
    "BIG-IP": {
        "os_release": "10.2.2"
    },
}
aos = None


def available_os():
    global aos
    if aos is None:
        aos = get_available_oss()
    return aos


def fill_defaults(opts):
    for key, default_value in guest_defaults.iteritems():
        opts.setdefault(key, default_value)
    opts.setdefault("os_release", os_defaults.get(opts["os_name"], {}).get("os_release"))
    return opts


########################################################################################################################
# Guest
########################################################################################################################

def _display_vm_info(guest):
    class vm_attr(object):
        def __init__(self, label, value, option=None):
            super(vm_attr, self).__init__()
            self.label = label
            self.value = value
            self.option = option

    display_table(
        [
            vm_attr("Name", guest.qualified_name),
            vm_attr("UUID", guest.uuid),
            vm_attr("State", guest.state),
            vm_attr("Host", guest.host.qualified_name if guest.host else ''),
            vm_attr("DomID", guest.dom_id if guest.host else ''),
            vm_attr("VMM Model", guest.vmm.model),
            vm_attr("Device Model", guest.device_model),
            vm_attr("Target OS", str(guest.os).replace('_', ' ')),
            vm_attr("Architecture", guest.arch),
            vm_attr("CPU Cores", guest.vcpus),
            vm_attr("Memory", si_units(guest.memory)),
        ], [
            DisplayField("Property", lambda a: a.label),
            DisplayField("Value", lambda a: a.value),
        ], group_by=DisplayField("Basic VM Parameters", None), group_headings=True,
    )
    display_table(
        [
            vm_attr("CPU Mapping", guest.cpus or '', "cpus"),
            vm_attr("CPU Weight", guest.cpu_weight or '', "cpu_weight"),
            vm_attr("CPU Cap", guest.cpu_cap or '', "cpu_cap"),
            vm_attr("Max Memory", guest.maxmem or '', "maxmem"),
            vm_attr("Shadow Memory", guest.shadow_memory or '', "shadow_memory"),
            vm_attr("Serial", guest.serial or '', "serial"),
            vm_attr("Console", guest.console or '', "console"),
            vm_attr("Loader", guest.loader or '', "loader"),
            vm_attr("Boot Loader Arguments", guest.bootloader_args or '', "bootloader_args"),
            vm_attr("Kernel Extra Arguments", guest.kernel_extra_args or '', "kernel_extra_args"),
            vm_attr("Enable Hardware Assisted Paging", guest.hap, "hap"),
            vm_attr("Suppress Spurious Page Faults", guest.suppress_spurious_page_faults,
                    "suppress_spurious_page_faults"),
            vm_attr("Enable Xen Platform PCI", guest.enable_xen_platform_pci, "enable_xen_platform_pci"),
            vm_attr("Enable ACPI", guest.enable_acpi, "enable_acpi"),
            vm_attr("Enable APIC", guest.enable_apic, "enable_apic"),
            vm_attr("Enable PAE", guest.enable_pae, "enable_pae"),
            vm_attr("Enable HPET", guest.enable_hpet, "enable_hpet"),
            vm_attr("Enable MS Virtualization Support", guest.enable_viridian, "enable_viridian"),
            vm_attr("Enable Nested HVM", guest.enable_nested_hvm, "enable_nested_hvm"),
            vm_attr("Enable Graphics", guest.enable_graphics, "enable_graphics"),
            vm_attr("Enable SDL", guest.enable_sdl, "enable_sdl"),
            vm_attr("Enable OpenGL", guest.enable_opengl, "enable_opengl"),
            vm_attr("Use Standard VGA Emulation", guest.stdvga, "stdvga"),
            vm_attr("Video Memory", guest.video_memory or '', "video_memory"),
            vm_attr("Sound Hardware", guest.sound_hw or '', "sound_hw"),
            vm_attr("Power-off Action", guest.on_poweroff or '', "on_poweroff"),
            vm_attr("Reboot Action", guest.on_reboot or '', "on_reboot"),
            vm_attr("Crash Action", guest.on_crash or '', "on_crash"),
        ], [
            DisplayField("Property", lambda a: a.label),
            DisplayField("Value", lambda a: a.value),
            DisplayField("Advanced Option Key", lambda a: a.option),
        ], group_by=DisplayField("Advanced VM Parameters", None), group_headings=True,
    )
    display_table(
        [
            vm_attr("Enable VNC", guest.vnc, "vnc"),
            vm_attr("Auto Port Allocation", guest.vnc_auto_port, "vnc_auto_port"),
            vm_attr("VNC Server Bind Address", guest.vnc_listen or '', "vnc_listen"),
            vm_attr("Password", guest.vnc_password or '', "vnc_password"),
            vm_attr("Console", guest.vnc_console, "vnc_console"),
            vm_attr("Display Index", guest.vnc_display or '', "vnc_display"),
        ], [
            DisplayField("Property", lambda a: a.label),
            DisplayField("Value", lambda a: a.value),
            DisplayField("Advanced Option Key", lambda a: a.option),
        ], group_by=DisplayField("Network Console Parameters", None), group_headings=True,
    )


class CreateGuest(Command):
    """Create a new virtual machine."""

    @classmethod
    def configure_parser(cls, parser):
        """
        :type parser: argparse.ArgumentParser
        """
        parser.add_argument("name", help="A tenant-qualified name (tenant:guest) for the guest.")
        parser.add_argument(
            "-t", dest="template", default=argparse.SUPPRESS,
            help="The name of a template on which to base this guest."
        )
        parser.add_argument(
            "-H", dest="hvm", action="store_true", default=argparse.SUPPRESS,
            help="Use a virtualised hardware (HVM) VMM model.\n"
                 "(default: para-virtualised guest-kernel-supported (PVM))"
        )
        parser.add_argument(
            '-a', dest='arch', default=argparse.SUPPRESS, choices=['i386', 'x86_64'],
            help='CPU architecture. (default: %s)' % guest_defaults['arch']
        )
        parser.add_argument(
            '-c', dest='vcpus', type=int, default=argparse.SUPPRESS, choices=range(1, 5),
            help='Virtual CPU core count. (default: %i)' % guest_defaults['vcpus']
        )
        parser.add_argument(
            '-m', dest='memory', metavar='MEMORY', type=int, default=argparse.SUPPRESS,
            help='Memory capacity in megabytes. (default: %i)' % guest_defaults['memory']
        )
        parser.add_argument(
            '-d', dest="disks", metavar="device:type:mode:size", action="append", default=argparse.SUPPRESS,
            help=textwrap.dedent("""\
            Specification for a hard disk. Use multiple times to configure multiple devices.
            Where the components have the following meanings:
            device:\t The name by which the virtual block device will be known to the guest.
            type:\t The block device type. {%s}
            mode:\t The access mode of the device. {r|w}
            size:\t The desired disk capacity in SI megabytes.
            """ % '|'.join(BlockDeviceType.values))
        )
        parser.add_argument(
            '-n', dest="nics", metavar="type:network:vlan[,vlan ...]", action="append", default=argparse.SUPPRESS,
            help=textwrap.dedent("""\
            Add a network interface with the supplied specification.
            Where the components have the following meanings:
            type:       The emulated hardware type. Supported types are:
                        {%s}
                        (default: netfront)
            network:    The qualified name of an existing network.
            vlan:       The name of an existing VLAN defined on 'network'.

            All components are optional (the colons must be present).
            Use multiple times to configure multiple interfaces.
            """ % ('|'.join(GuestInterfaceDriver.values))
            )
        )
        parser.add_argument(
            "-o", dest="os_name", choices=sorted(set([os.name for os in available_os()])),
            default=argparse.SUPPRESS,
            help="Target OS. Used to select sane VMM defaults. (default: %s)" % guest_defaults['os_name']
        )
        parser.add_argument(
            "-r", dest="os_release", default=argparse.SUPPRESS,
            help="OS release. e.g. 5.7 or 2003-R2. (default: %s)" %
                 os_defaults.get(guest_defaults["os_name"])["os_release"]
        )
        parser.add_argument(
            "-p", dest="vnc_password", metavar="PASSWORD", default=argparse.SUPPRESS,
            help="Password for console access over VNC. (default: %s)" % guest_defaults["vnc_password"]
        )
        parser.add_argument('--force', action='store_true', default=False, help='''Push it baby.''')
        advanced = parser.add_argument_group("key-value arguments", description="advanced options as key=value pairs")
        advanced.add_argument("advanced", nargs=argparse.ZERO_OR_MORE, metavar="key=value",
                              help="A key=value tuple which will be passed through to the guest service via **kwargs.")

    def execute(self, name, template=None, force=False, advanced=None, disks=None, nics=None, **kwargs):
        def device_spec(device_string):
            try:
                device, device_type, mode, size = device_string.split(':', 3)
                return BlockDeviceSpec(device, device_type, (mode == 'r'), int(size) if size else None)
            except:
                raise XenmasterError(
                    "Invalid disk device specification: %s" % device_string,
                    textwrap.dedent("""\
                    Disk device specifications must be of the form device:type:mode:size, where
                    component attributes have the following meanings:

                    device: The name by which the virtual block device will be known to the
                             guest (e.g. xvda, sdb, hdc).
                    type:   The block device type.
                             {%s}
                    mode:   The access mode of the device. {r|w}
                    size:   The desired disk capacity in SI megabytes.""" % '|'.join(BlockDeviceType.values)))

        def _parse_nic(device_string):
            """
            :param device_string: Device string of the form 'type:network:vlan[,vlan ...]'
            :type device_string: string
            :returns: List of network devices
            :rtype: (GuestInterfaceDriver, Network, list of VLAN)
            """
            try:
                driver_name, network_name, vlan_names = device_string.split(':')
                if driver_name:
                    driver = GuestInterfaceDriver.values[str(driver_name)]
                else:
                    driver = GuestInterfaceDriver.netfront
                network, _ = parse_network_name_hard(network_name or "trunk")
                vlans = []
                for vlan_name in vlan_names.split(","):
                    vlan = get_vlan_with_name(vlan_name, network)
                    if vlan:
                        vlans.append(vlan)
                    else:
                        raise

                return driver, network, vlans
            except:
                raise XenmasterError(
                    "Invalid network device specification: %s" % device_string,
                    textwrap.dedent("""\
                    Network device specifications must be of the form 'type:network:vlan[,vlan ...]', where
                    component attributes have the following meanings:

                    type:       The emulated hardware type.
                                {%s}
                    network:    The qualified name of an existing network.
                    vlan:       The name of an existing VLAN defined on 'network'.

                    All components are optional (the colon must be present).
                    """ % '|'.join(GuestInterfaceDriver.values)))

        tenant_name, _, guest_name = name.rpartition(':')
        tenant = parse_tenant_name(tenant_name)
        if tenant.cluster is None:
            raise XenmasterError("Unable to create '%s'." % name,
                                 "The tenant '%s' is not associated with any cluster." % tenant.shortname)

        if template:
            _template, template_tenant = parse_guest_template_name(template)
            if _template is None:
                raise XenmasterError("Unable to find a template named '%s'." % template)
        else:
            _template, template_tenant = parse_guest_template_name(name)
            if _template:
                info("No template was specified, but a template named '%s' was found and will be used."
                     % _template.name)
            else:
                fill_defaults(kwargs)

        guest = get_guest_with_name(guest_name, tenant)
        if guest:
            if force:
                delete_guest(guest)
            else:
                raise XenmasterError(
                    "The '%s' tenant already has a guest with name '%s'" % (tenant.shortname, guest_name),
                    "If you are _certain_ that you want to destroy the existing VM and create a new one "
                    "use the --force option.")
        if advanced:
            for tup in advanced:
                key, value = tup.split('=', 1)
                if value:
                    kwargs[key] = value
        block_devices = [device_spec(disk) for disk in disks] if disks else None
        network_devices = [_parse_nic(nic) for nic in nics] if nics else None
        guest = create_guest(guest_name, tenant, _template, block_devices, network_devices, **kwargs)

        # for role in guest.roles:
        #     packages.update(role.get_packages(guest.os))
        #     post_install_scripts.extend(role.get_post_install_scripts(guest.os_release))
        # guest.bootloader_args = "ks=nfs:xenmaster:/media/rhel/%s/ks.cfg" % guest.arch


class ModifyGuest(Command):
    __doc__ = textwrap.dedent("""\
    Modify VM properties of an existing guest, by
    supplying new values for attributes to be updated.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument('name', default=argparse.SUPPRESS, help='''
            The tenant-qualified name of the guest to modify.
        ''')
        parser.add_argument(
            "-H", dest="hvm", action="store_true", default=argparse.SUPPRESS,
            help="Use a virtualised hardware (HVM) VMM model."
        )
        parser.add_argument(
            '-a', dest='arch', default=argparse.SUPPRESS, choices=['i386', 'x86_64'],
            help='CPU architecture.'
        )
        parser.add_argument(
            '-c', dest='vcpus', type=int, default=argparse.SUPPRESS, choices=range(1, 5),
            help='Virtual CPU core count.'
        )
        parser.add_argument(
            '-m', dest='memory', metavar='MEMORY', type=int, default=argparse.SUPPRESS,
            help='Memory capacity in megabytes.'
        )
        parser.add_argument(
            "-o", dest="os_name", choices=sorted(set([os.name for os in available_os()])),
            default=argparse.SUPPRESS,
            help="Target OS. Used to select sane VMM defaults."
        )
        parser.add_argument(
            "-r", dest="os_release", default=argparse.SUPPRESS,
            help="OS release. e.g. 5.7 or 2003-R2."
        )
        parser.add_argument(
            "-p", dest="vnc_password", metavar="PASSWORD", default=argparse.SUPPRESS,
            help="Password for console access over VNC."
        )
        advanced = parser.add_argument_group("key-value arguments", description="advanced options as key=value pairs")
        advanced.add_argument("advanced", nargs=argparse.ZERO_OR_MORE, metavar="key=value",
                              help="A key=value tuple which will be passed through to the guest service via **kwargs.")

    def execute(self, name, advanced=None, **kwargs):
        if advanced:
            for tup in advanced:
                key, value = tup.split('=', 1)
                kwargs[key] = value
        guest = modify_guest(name, **kwargs)
        info("%s has been modified. The updated VM configuration is as follows:" % guest.qualified_name)
        _display_vm_info(guest)


class StartGuest(Command):
    """Start an existing virtual machine."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-n", "--no_monitor", dest="monitor", action="store_false", default=True,
                            help="Do not spawn a monitor process to wait for guest shutdown.")
        parser.add_argument("-c", "--console", action="store_true", default=False,
                            help="Connect a VNC client to the guest console after start.")
        parser.add_argument("guest", help="The tenant-qualified name of the guest (guest-name.tenant-name)")
        parser.add_argument("host", nargs="?", help="Optionally specified host to attempt to start the guest on.")

    def execute(self, guest, host=None, console=False, monitor=True):
        guest, tenant = parse_guest_name_hard(guest)
        sync_cluster_state(tenant.cluster)
        target_host = get_host_with_name(host, tenant.cluster)
        start_guest(guest, target_host, monitor)
        if console:
            connect_to_guest_console(guest)


class StopGuest(Command):
    """Stop a running virtual machine."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-t", "--timeout", type=int, default=300,
                            help=textwrap.dedent("""\
                            Wait a maximum of TIMEOUT seconds for the guest to stop before giving up or, if the
                            force option was used, killing it via a non-maskable interrupt. (default: 300)
                            """))
        parser.add_argument("guest", help="The tenant-qualified name of the guest (guest-name.tenant-name)")
        parser.add_argument("--force", action='store_true', default=False,
                            help="'destroy' the guest domain if it fails to respond to 'shutdown'")
        # parser.add_argument("-w", "--wait", action='store_true', default=False,
        #                     help="Do not return until the guest has finished shutting down.")

    def execute(self, guest, timeout, force=False, **kwargs):
        guest, tenant = parse_guest_name_hard(guest)
        sync_cluster_state(tenant.cluster)
        stop_guest(guest, force, timeout)


class RestartGuest(Command):
    """Reboot a running virtual machine."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-c", "--console", action="store_true", default=False,
                            help="Connect a VNC client to the guest console after start.")
        parser.add_argument("--force", action='store_true', default=False,
                            help="'destroy' the guest domain if it fails to respond to 'shutdown'")
        parser.add_argument("guest", help="The tenant-qualified name of the guest (guest-name.tenant-name)")
        parser.add_argument("host", nargs="?",
                            help="Optionally specify a different host on which to restart the guest.")

    def execute(self, guest, force=False, host=None, console=False, **kwargs):
        guest, tenant = parse_guest_name_hard(guest)
        sync_cluster_state(tenant.cluster)
        if host:
            target_host = get_host_with_name(host, tenant.cluster)
        else:
            target_host = guest.host
        stop_guest(guest, force)
        start_guest(guest, target_host)
        if console:
            connect_to_guest_console(guest)


# class PauseGuest(Command):
#     """Pause (sleep to disk) a running virtual machine."""
#
#     @classmethod
#     def configure_parser(cls, parser):
#         parser.add_argument('name', help='''The short name given to the guest when it was created.''')


# class ResumeGuest(Command):
#     """Resume a previously paused virtual machine."""
#
#     @classmethod
#     def configure_parser(cls, parser):
#         parser.add_argument('name', help='''The short name given to the guest when it was created.''')


# class MigrateGuest(Command):
#     """'hot' migrate a running virtual machine from it's current host to another host within the same cluster"""
#
#     @classmethod
#     def configure_parser(cls, parser):
#         parser.add_argument('name', help='''The short name given to the guest when it was created.''')


class DeleteGuest(Command):
    __doc__ = textwrap.dedent("""\
    Remove all trace (disk images and config) of an existing
    (and possibly running) virtual machine.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="The tenant-qualified name of the guest (guest-name.tenant-name)")
        parser.add_argument('--force', action='store_true', default=False, help='''push it baby''')

    def execute(self, name, force=False, **kwargs):
        guest, tenant = parse_guest_name_hard(name)
        sync_cluster_state(tenant.cluster)
        if guest.dom_id:
            if not force:
                raise XenmasterError("Refusing to delete %s as it is currently running on %s" % (name, guest.host.name))
            stop_guest(guest)
        delete_guest(guest)


class GuestConsole(Command):
    """Connect to a running guest's VNC console."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="The tenant-qualified name of the guest (guest-name.tenant-name)")

    def execute(self, name, **kwargs):
        guest, tenant = parse_guest_name_hard(name)
        if not guest.host:
            raise XenmasterError("Unable to connect to %s's console." % guest.qualified_name,
                                 "'%s' is not running." % guest.qualified_name)
        sync_guest_state(guest.host)
        connect_to_guest_console(guest)


class ListGuests(Command):
    __doc__ = textwrap.dedent("""\
    List all virtual machines deployed or configured, optionally
    limiting the search scope.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("scope", metavar="tenant|host|cluster", nargs=argparse.OPTIONAL,
                            help="Optional tenant, host or cluster name to which the search scope will be constrained.")

    def execute(self, scope=None, **kwargs):
        guests = []
        group_by = None
        if scope:
            host_name, _, cluster_name = scope.rpartition('.')
            cluster = get_cluster_with_name(cluster_name)
            if cluster:
                sync_cluster_state(cluster)
                host = get_host_with_name(host_name, cluster)
                if host:
                    guests.extend(host.guests)
                else:
                    group_by = DisplayField(lambda h: "%s guests" % h, lambda g: g.host.name)
                    for host in sorted(cluster.hosts, key=lambda h: h.index):
                        guests.extend(host.guests)
            else:
                tenant = parse_tenant_name(scope)
                if tenant.cluster:
                    sync_cluster_state(tenant.cluster)
                guests.extend(tenant.guests)
        else:
            group_by = DisplayField(lambda t: "%s guests" % t, lambda g: g.tenant.shortname)
            for cluster in get_clusters():
                sync_cluster_state(cluster)
            for tenant in sorted(get_tenants(), key=lambda t: t.name):
                guests.extend(tenant.guests)
        if len(guests) == 0:
            print("%s has no guests" % scope)
            return

        display_table(
            guests, [
                DisplayField("Name", lambda g: g.name),
                DisplayField("State", lambda g: g.state),
                DisplayField("Host", lambda g: g.host.qualified_name if g.host else ''),
                DisplayField("DomID", lambda g: g.dom_id if g.host else '', justify='r'),
                DisplayField("VMM model", lambda g: g.vmm.model),
                DisplayField("OS", lambda g: str(g.os).replace('_', ' ')),
                DisplayField("Arch", lambda g: g.arch),
                DisplayField("Cores", lambda g: g.vcpus, justify='r'),
                DisplayField("Memory (MB)", lambda g: g.memory, justify='r'),
                DisplayField("Storage (committed)",
                             lambda g: si_units(sum(b.disk_image.virtual_size
                                                    for b in g.block_devices.values()
                                                    if b.disk_image and not b.disk_image.shareable)),
                             justify='r'),
            ],
            group_by=group_by,
            order_by=lambda g: g.name, group_headings=False
        )


class GuestDetail(Command):
    __doc__ = textwrap.dedent("""\
    Display details of a guest's VM spec, state and any defined
    block devices, network interfaces and disk images.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="The tenant-qualified name of the guest (tenant:guest)")

    def execute(self, name, **kwargs):
        guest, tenant = parse_guest_name_hard(name)
        sync_cluster_state(tenant.cluster)
        _display_vm_info(guest)
        _display_block_device_info(guest)
        _display_network_device_info(guest)


class GuestConfig(Command):
    __doc__ = textwrap.dedent("""\
    Display a guest's rendered config in a format suitable for
    consumption by XL.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="The tenant-qualified name of the guest (guest-name.tenant-name)")

    def execute(self, name, **kwargs):
        guest, _ = parse_guest_name_hard(name)
        print guest.render_config()


########################################################################################################################
# Block Devices
########################################################################################################################

def _display_block_device_info(guest):
    display_table(
        guest.block_devices.values(), [
            DisplayField("Name", lambda d: d.name),
            DisplayField("Type", lambda d: d.device_type),
            DisplayField("Mode", lambda d: "r" if d.read_only else "w"),
            DisplayField("Image", lambda d: d.disk_image.qualified_name if d.disk_image else ''),
            DisplayField("Boot", lambda d: 'X' if d is d.guest.boot_device else ''),
            DisplayField("Connected", lambda d: 'X' if d.connected else ''),
            DisplayField("Locked", lambda d: 'X' if d.storage_locked else ''),
            # DisplayField("Driver", lambda d: d.driver),
            DisplayField("Status", lambda d: d.status_description),
            DisplayField("Capacity",
                          lambda d: si_units(d.disk_image.virtual_size if d.disk_image else d.capacity
                          if d.capacity else 0),
                          justify='r'),
        ], group_by=DisplayField("Block Devices", None), group_headings=True,
        order_by=lambda d: d.name
    )


class AddNewHardDisk(Command):
    __doc__ = textwrap.dedent("""\
    Allocate a new disk image within a storage repository, and
    associate it with a guest as a new virtual block device.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-r", "--repo",
                            help=textwrap.dedent("""\
                            The tenant accessible repository in which to create the new disk image.
                            If unspecified the tenant's default repository will be used.
                            """))
        parser.add_argument("guest", help="The tenant-qualified name of the guest (guest-name.tenant-name).")
        parser.add_argument("device", help="The name by which the virtual block device will be known to the guest.")
        parser.add_argument("capacity", type=int, help="The disk image size in megabytes (1024^2 bytes).")
        parser.add_argument("name", nargs=argparse.OPTIONAL,
                            help=textwrap.dedent("""\
                            A name for the created disk image. If not supplied, a name will be generated from "
                            the guest name and device.
                            """))

    def execute(self, guest, device, capacity, name=None, repo=None, **kwargs):
        guest, tenant = parse_guest_name_hard(guest)
        repository = get_storage_repository_with_name(repo, tenant) \
            if repo else tenant.default_storage_repository
        image_name = name if name else "%s-%s" % (guest.safe_qualified_name, device)
        with transaction():
            disk_image = create_disk_image(image_name, repository, capacity, shareable=False, read_only=False)
            disk = create_virtual_block_device(
                guest, BlockDeviceSpec(device, BlockDeviceType.hdd, False, disk_image.virtual_size), disk_image)
            if guest.is_running:
                connect_virtual_block_device(disk, guest.host)
        info("%s has been modified. The updated block device configuration is as follows:" % guest.qualified_name)
        _display_block_device_info(guest)


class AddExistingHardDisk(Command):
    __doc__ = textwrap.dedent("""\
    Associate an existing disk image with a guest as a new
    virtual block device.
    """)

    @classmethod
    def configure_parser(cls, parser):
        """
        :type parser: argparse.ArgumentParser
        """
        parser.add_argument("-m", "--mode", choices=['r', 'w'], default='w',
                            help=textwrap.dedent("""\
                            The access mode of the virtual block device. This is ignored if the disk will be shared
                            and the existing image's access mode is read-only.
                            """))
        copy = parser.add_argument_group()
        copy.add_argument("-c", "--copy", action="store_true", default=False,
                          help=textwrap.dedent("""\
                          Make a separate copy of the virtual disk image. The default behaviour is to share the
                          existing disk image read-only.
                          """))
        copy.add_argument("name", nargs=argparse.OPTIONAL,
                          help=textwrap.dedent("""\
                          Optional name for a copied disk image. If 'name' is qualified with the name of a
                          storage repository accessible to the tenant, the copy will be created in the named
                          storage repository. If 'name' is unqualified the tenant's default repository will be
                          used. If a copy is required and 'name' is not supplied, a name will be generated from
                          the guest name and device.
                          """))
        parser.add_argument("guest", help="The tenant-qualified name of the guest (guest-name.tenant-name).")
        parser.add_argument("device", help="The name by which the virtual block device will be known to the guest.")
        parser.add_argument("image", help="The repository-qualified disk image name (repository/image).")

    def execute(self, guest, device, image, mode, copy=False, name=None, **kwargs):
        guest, tenant = parse_guest_name_hard(guest)
        repo_name, _, image_name = image.rpartition('/')
        if repo_name:
            repository = get_storage_repository_with_name(repo_name, tenant)
        else:
            repository = tenant.default_storage_repository
        disk_image = get_disk_image_with_name(image_name, repository)
        if disk_image is None:
            raise XenmasterError("No disk image '%s' could be found in '%s'" % (image_name, repository.name))
        with transaction():
            if copy:
                # define the parameters for an image copy
                dest_repo = tenant.default_storage_repository
                read_only = (mode == 'r')
                if name:
                    repo_name, _, image_name = name.rpartition('/')
                    if repo_name:
                        dest_repo = get_storage_repository_with_name(repo_name, tenant)
                else:
                    image_name = "%s-%s" % (guest.safe_qualified_name, device)
                disk_image = copy_disk_image(disk_image, image_name, dest_repo, read_only)
            else:
                read_only = (mode == 'r') or disk_image.read_only

            spec = BlockDeviceSpec(device, BlockDeviceType.hdd, read_only, disk_image.virtual_size)
            disk = create_virtual_block_device(guest, spec, disk_image)
            if guest.is_running:
                connect_virtual_block_device(disk, guest.host)
        info("%s has been modified. The updated block device configuration is as follows:" % guest.qualified_name)
        _display_block_device_info(guest)


class ImportExistingHardDisk(Command):
    __doc__ = textwrap.dedent("""\
    Import an existing physical block device as a disk image,
    and associate it with a guest as a new virtual block device.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-m", "--mode", choices=['r', 'w'], default='r',
                            help="The access mode of the disk image. (default: r)")
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest)")
        parser.add_argument("device", help="The name by which the virtual block device will be known to the guest.")
        parser.add_argument("path", help="The full path (host.cluster:path) to the existing physical block device.")
        parser.add_argument("name", nargs=argparse.OPTIONAL,
                            help=textwrap.dedent("""\
                            If specified, a copy of 'path' will be created with the supplied 'name'.
                            If 'name' is qualified with the name of a storage repository, the copy will be
                            created in the named storage repository. If 'name' is unqualified the tenant's
                            default storage repository will be used.
                            If a copy is required, because 'path' lies outside any tenant storage repository,
                            and 'name' is not supplied, the image will be created in the tenant's default
                            storage repository with a name generated from the guest name and device.
                            """))

    def execute(self, guest, device, path, mode, name=None, **kwargs):
        guest, tenant = parse_guest_name_hard(guest)
        host, _, _path = path.partition(':')
        if host is '' or _path is '':
            raise XenmasterError("Unable to import '%s'" % path,
                                 "The supplied path was not of the form host.cluster:path.")
        host_name, _, cluster_name = host.partition('.')
        if not cluster_name:
            raise XenmasterError("Path host was not qualified with a cluster name.")
        cluster = get_cluster_with_name(cluster_name)
        cluster_node = get_host_with_name(host_name, cluster)
        read_only = (mode == 'r')
        dest_repo = tenant.default_storage_repository
        image_name = "%s-%s" % (guest.safe_qualified_name, device)
        if name:
            repo_name, _, image_name = name.rpartition('/')
            if repo_name:
                dest_repo = get_storage_repository_with_name(repo_name, tenant)
        else:
            candidate_repos = get_storage_repositories_for_tenant(tenant)
            for repo in candidate_repos:
                if _path.startswith(os.path.join(str(repo.path), '')):
                    dest_repo = repo
                    image_name = os.path.relpath(_path, str(repo.path))
                    break
        with transaction():
            disk_image = import_disk_image(cluster_node, _path, image_name, dest_repo, read_only=read_only)
            spec = BlockDeviceSpec(device, BlockDeviceType.hdd, read_only, disk_image.virtual_size)
            disk = create_virtual_block_device(guest, spec, disk_image)
            if guest.is_running:
                connect_virtual_block_device(disk, guest.host)
        info("%s has been modified. The updated block device configuration is as follows:" % guest.qualified_name)
        _display_block_device_info(guest)


class AddOpticalDrive(Command):
    """Create a virtual optical block device on guest."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest)")
        parser.add_argument("device", help="The guest device name")

    def execute(self, guest, device, **kwargs):
        guest, tenant = parse_guest_name_hard(guest)
        cdrom = create_virtual_block_device(guest, BlockDeviceSpec(device, BlockDeviceType.cdrom, True, None))
        if guest.is_running:
            connect_virtual_block_device(cdrom, guest.host)
        info("%s has been modified. The updated block device configuration is as follows:" % guest.qualified_name)
        _display_block_device_info(guest)


class InsertRemovableMedia(Command):
    """'Insert' a removable media disk image into the named device."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest)")
        parser.add_argument("device", help="The guest device name")
        parser.add_argument("image", help="The repository-qualified disk image name (repository/image).")

    def execute(self, guest, device, image, **kwargs):
        guest, tenant = parse_guest_name_hard(guest)
        dev = guest.block_devices.get(device)
        if dev is None:
            raise XenmasterError("'%s' has no device '%s'" % (guest.qualified_name, device))
        repo_name, _, image_name = image.rpartition('/')
        if repo_name:
            repo = get_storage_repository_with_name(repo_name, tenant)
        else:
            repo = tenant.default_storage_repository
        media = get_disk_image_with_name(image_name, repo)
        if media is None:
            raise XenmasterError("No disk image '%s' could be found in '%s'" % (image_name, repo.name))
        insert_disk_image(dev, media)
        info("%s has been modified. The updated block device configuration is as follows:" % guest.qualified_name)
        _display_block_device_info(guest)


class EjectRemovableMedia(Command):
    """'Eject' a removable media disk image from the named device."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest)")
        parser.add_argument("device", help="The guest device name")

    def execute(self, guest, device, **kwargs):
        guest, tenant = parse_guest_name_hard(guest)
        dev = guest.block_devices.get(device)
        if dev is None:
            raise XenmasterError("'%s' has no device '%s'" % (guest.qualified_name, device))
        eject_disk_image(dev)
        info("%s has been modified. The updated block device configuration is as follows:" % guest.qualified_name)
        _display_block_device_info(guest)


class RenameBlockDevice(Command):
    """Rename a guest virtual block device."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest).")
        parser.add_argument("device", help="The current guest device name.")
        parser.add_argument("new_name", help="The new guest device name.")

    def execute(self, guest, device, new_name, **kwargs):
        guest, tenant = parse_guest_name_hard(guest)
        block_device = guest.block_devices.get(device)
        if block_device is None:
            raise XenmasterError("%s has no block device %s" % guest.qualified_name, device)
        if guest.is_running:
            disconnect_virtual_block_device(block_device, guest.host)
        rename_virtual_block_device(block_device, new_name)
        if guest.is_running:
            connect_virtual_block_device(block_device, guest.host)
        info("%s has been modified. The updated block device configuration is as follows:" % guest.qualified_name)
        _display_block_device_info(guest)


class RemoveBlockDevice(Command):
    """Remove a guest virtual block device."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest).")
        parser.add_argument("device", help="The guest device name.")

    def execute(self, guest, device, **kwargs):
        guest, tenant = parse_guest_name_hard(guest)
        block_device = guest.block_devices.get(device)
        if block_device is None:
            raise XenmasterError("%s has no block device %s" % guest.qualified_name, device)
        if guest.is_running:
            disconnect_virtual_block_device(block_device, guest.host)
        delete_virtual_block_device(block_device)
        info("%s has been modified. The updated block device configuration is as follows:" % guest.qualified_name)
        _display_block_device_info(guest)


class DeleteBlockDevice(Command):
    """Delete a guest virtual block device and its backing disk image."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest).")
        parser.add_argument("device", help="The guest device name.")

    def execute(self, guest, device, **kwargs):
        guest, tenant = parse_guest_name_hard(guest)
        block_device = guest.block_devices.get(device)
        if block_device is None:
            raise XenmasterError("%s has no block device %s" % (guest.qualified_name, device))
        image = block_device.disk_image
        if len(image.block_devices) > 1:
            raise XenmasterError("Refusing to delete a shared disk image")
        if guest.is_running:
            disconnect_virtual_block_device(block_device, guest.host)
        delete_virtual_block_device(block_device)
        delete_disk_image(image)
        info("%s has been modified. The updated block device configuration is as follows:" % guest.qualified_name)
        _display_block_device_info(guest)


class SetBootDevice(Command):
    """Set a guest's boot device."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest)")
        parser.add_argument("device", help="The guest device name")

    def execute(self, guest, device, **kwargs):
        guest, tenant = parse_guest_name_hard(guest)
        set_guest_boot_device(guest, device)
        info("%s has been modified. The updated block device configuration is as follows:" % guest.qualified_name)
        _display_block_device_info(guest)


class ListBlockDevices(Command):
    """List a guest's virtual block devices."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest).")

    def execute(self, guest, **kwargs):
        guest, _ = parse_guest_name_hard(guest)
        _display_block_device_info(guest)


########################################################################################################################
# Network Devices
########################################################################################################################

def _display_network_device_info(guest):
    display_table(
        guest.network_interfaces, [
            DisplayField("Index", lambda d: d.index),
            DisplayField("MAC", lambda d: d.mac),
            DisplayField("Network", lambda d: d.network.qualified_name if d.network else ''),
            DisplayField("VLANs", lambda d: ', '.join(v.name for v in sorted(d.vlans))),
            DisplayField("Connected", lambda d: 'X' if d.connected else ''),
            DisplayField("Driver", lambda d: d.driver),
            DisplayField("Rate Limit", lambda d: d.rate),
        ], group_by=DisplayField("Network Interfaces", None), group_headings=True,
        order_by=lambda d: d.index
    )


class AddNetworkInterface(Command):
    """Create a new guest network interface."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-d", dest="driver", choices=GuestInterfaceDriver.values, help=textwrap.dedent("""\
                            The hardware emulation model to present to the guest.
                            (default: %s)""" % GuestInterfaceDriver.netfront))
        parser.add_argument("-m", dest="mac", help=textwrap.dedent("""\
            MAC address for the interface. If not supplied one will be
            randomly generated.
        """))
        parser.add_argument("guest", help="The tenant-qualified name of the guest to modify.")
        parser.add_argument("network", help="e.g. test.")
        parser.add_argument("vlans", metavar="vlan", nargs="*",
                            help="A list (possibly empty) of names of existing VLANs available on the named network.")

    def execute(self, guest, network, mac=None, driver=None, vlans=None):
        create_guest_interface(guest, driver, network, vlans, mac)
        info("%s has been modified. The updated network device configuration is as follows:" % guest)
        guest, _ = parse_guest_name_hard(guest)
        _display_network_device_info(guest)


class BindNetworkInterface(Command):
    """Associate a network interface with a network, optionally specifying one or more VLANs."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest).")
        parser.add_argument("index", type=int, choices=range(6),
                            help="The index of the target among the guest's list of interfaces.")
        parser.add_argument("network", help="The switch network to which the nic will connect. e.g. test:trunk")
        parser.add_argument("vlans", metavar="vlan", nargs="*",
                            help="A list (possibly empty) of names of existing VLANs available on the named network.")

    def execute(self, guest, index, network, vlans=None):
        bind_guest_interface(guest, index, network, vlans)
        info("%s has been modified. The updated network device configuration is as follows:" % guest)
        guest, _ = parse_guest_name_hard(guest)
        _display_network_device_info(guest)


class UnbindNetworkInterface(Command):
    """Dissociate a network interface from any network."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest).")
        parser.add_argument("index", type=int, choices=range(6),
                            help="The index of the target among the guest's list of interfaces.")

    def execute(self, guest, index):
        bind_guest_interface(guest, index, None)
        info("%s has been modified. The updated network device configuration is as follows:" % guest)
        guest, _ = parse_guest_name_hard(guest)
        _display_network_device_info(guest)


class RemoveNetworkInterface(Command):
    """Remove a network interface from a guest."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest).")
        parser.add_argument("index", type=int, choices=range(6),
                            help="The index of the target among the guest's list of interfaces.")

    def execute(self, guest, index):
        delete_guest_interface(guest, index)
        info("%s has been modified. The updated network device configuration is as follows:" % guest)
        guest, _ = parse_guest_name_hard(guest)
        _display_network_device_info(guest)


class ListNetworkInterfaces(Command):
    """List a guest's network interfaces."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("guest", help="The tenant-qualified name of the guest (tenant:guest).")

    def execute(self, guest, **kwargs):
        guest, _ = parse_guest_name_hard(guest)
        _display_network_device_info(guest)


meta = AttributedDict({
    "help": __doc__,
    "actions": [
        (ANSI.BOLD_ON + "Basic actions" + ANSI.RESET, [
            ("create", CreateGuest),
            ("modify", ModifyGuest),
            ("start", StartGuest),
            ("stop", StopGuest),
            ("restart", RestartGuest),
            # ("pause", PauseGuest),
            # ("resume", ResumeGuest),
            # ("migrate", MigrateGuest),
            ("delete", DeleteGuest),
            ("console", GuestConsole),
            ("detail", GuestDetail),
            ("config", GuestConfig),
            ("list", ListGuests),
        ]),
        (ANSI.BOLD_ON + "Block device actions" + ANSI.RESET, [
            ("create-hdd", AddNewHardDisk),
            ("add-hdd", AddExistingHardDisk),
            ("import-hdd", ImportExistingHardDisk),
            ("add-cdrom", AddOpticalDrive),
            ("insert-media", InsertRemovableMedia),
            ("eject-media", EjectRemovableMedia),
            ("set-boot-device", SetBootDevice),
            ("rename-block-device", RenameBlockDevice),
            ("remove-block-device", RemoveBlockDevice),
            ("delete-block-device", DeleteBlockDevice),
            ("list-block-devices", ListBlockDevices),
        ]),
        (ANSI.BOLD_ON + "Network interface actions" + ANSI.RESET, [
            ("add-nic", AddNetworkInterface),
            ("bind-nic", BindNetworkInterface),
            ("unbind-nic", UnbindNetworkInterface),
            ("remove-nic", RemoveNetworkInterface),
            ("list-nics", ListNetworkInterfaces),
        ]),
    ]
})
