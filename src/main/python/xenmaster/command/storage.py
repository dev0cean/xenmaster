import argparse
import os
import textwrap
from ocsutil import AttributedDict, si_units
from ocsutil.console import display_table, DisplayField, warn, info, Command, _print

from libxm.exceptions import XenmasterError
from libxm.model import transaction
from libxm.model.storage import BlockDeviceType
from libxm.service.host_service import get_cluster_with_name, get_host_with_name
from libxm.service.guest_service import parse_tenant_name, get_global_tenant
from libxm.service.storage_service import connect_storage_repository, get_storage_repositories_for_tenant, \
    get_storage_repositories, delete_disk_image, parse_disk_image_name, parse_storage_repository_name, \
    import_disk_image, add_storage_repository, create_storage_repository, create_disk_image, \
    get_storage_repository_with_name, migrate_disk_image, copy_disk_image
import sys


class CreateDiskImage(Command):
    """Create a new empty disk image within the named repository."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-b", "--boot", action="store_true", default=False, help="Mark the device as bootable.")
        parser.add_argument("-m", "--mode", choices=['r', 'w'], default='r',
                            help="The access mode of the disk image. (default: r)")
        parser.add_argument("-t", "--type", choices=BlockDeviceType.values, default=BlockDeviceType.hdd,
                            help="Guest device type.")
        parser.add_argument("repository", help="The tenant-qualified name of the existing storage repository in which "
                                               "to create the new disk image.")
        parser.add_argument("name", help="A name for the image.")
        parser.add_argument("capacity", type=int, help="The disk image size in megabytes (1024^2 bytes).")

    def execute(self, repository, name, capacity, mode='r', **kwargs):
        repo, tenant = parse_storage_repository_name(repository)
        read_only = (mode == 'r')
        create_disk_image(name, repo, capacity, location=None, description=None,
                          shareable=read_only, read_only=read_only)


class ImportDiskImage(Command):
    """Import an existing physical block device as a disk image."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-m", "--mode", choices=['r', 'w'], default='r',
                            help="The access mode of the disk image. (default: r)")
        parser.add_argument("path", help="The full path (host.cluster:path) to the existing physical block device.")
        parser.add_argument("name", nargs=argparse.OPTIONAL, help=textwrap.dedent("""\
            If specified, a copy of 'path' will be created with the supplied 'name'.
            If 'name' is qualified with the name of a storage repository, the copy
            will be created in the named storage repository. If 'name' is unqualified
            the cluster's default storage repository will be used.
            If a copy is required, because 'path' lies outside any defined storage
            repository, and 'name' is not supplied, the image will be created in the
            cluster's default storage repository with a name equal to the last element
            of 'path'."""))

    def execute(self, path, mode, name=None, **kwargs):
        host, _, _path = path.rpartition(':')
        if host is '' or _path is '':
            raise XenmasterError("Unable to import '%s'" % path,
                                 "The supplied path was not of the form host.cluster:path.")
        host_name, _, cluster_name = host.partition('.')
        if not cluster_name:
            raise XenmasterError("Path host was not qualified with a cluster name.")
        cluster = get_cluster_with_name(cluster_name)
        if not cluster:
            raise XenmasterError("No cluster with name '%s'" % cluster_name)
        cluster_node = get_host_with_name(host_name, cluster)
        dest_repo = cluster.default_storage_repository
        image_name = os.path.basename(_path)
        read_only = (mode == 'r')
        if name:
            repo_name, _, image_name = name.rpartition('/')
            if repo_name:
                tenant_name, _, repo_name = repo_name.rpartition('.')
                tenant = parse_tenant_name(tenant_name)
                dest_repo = get_storage_repository_with_name(repo_name, tenant)
        else:
            candidate_repos = get_storage_repositories()
            for repo in candidate_repos:
                if _path.startswith(os.path.join(str(repo.path), '')):
                    dest_repo = repo
                    image_name = os.path.relpath(_path, str(repo.path))
                    break
        with transaction():
            import_disk_image(cluster_node, _path, image_name, dest_repo, read_only=read_only)


class CopyDiskImage(Command):
    """Copy an existing disk image to the same or another repository."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-m", "--mode", choices=['r', 'w'],
                            help="The access mode of the new disk image. (default: same as source)")
        parser.add_argument("name", help="The repository-qualified name (repo/image) of the source disk image.")
        parser.add_argument("copy", help="The repository-qualified name (repo/image) of the target disk image.")

    def execute(self, name, copy, mode=None, **kwargs):
        image = parse_disk_image_name(name)
        if image is None:
            info("The source disk image '%s' does not exist." % name)
            return
        old_qualified_name = image.qualified_name
        repo_name, _, image_name = copy.rpartition('/')
        if repo_name:
            tenant_name, _, repo_name = repo_name.rpartition('.')
            tenant = parse_tenant_name(tenant_name)
            dest_repo = get_storage_repository_with_name(repo_name, tenant)
        else:
            dest_repo = image.repository
        image = copy_disk_image(image, image_name, dest_repo, (mode == 'r') if mode else None)
        info("%s has been successfully copied to %s." % (old_qualified_name, image.qualified_name))


class MigrateDiskImage(Command):
    """Migrate an existing disk image to another repository."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="The repository-qualified name (repo/image) of the disk image to move.")
        parser.add_argument("repository", help="The tenant-qualified name of the destination storage repository.")

    def execute(self, name, repository, **kwargs):
        image = parse_disk_image_name(name)
        if image is None:
            info("Disk image '%s' does not exist." % name)
            return
        old_qualified_name = image.qualified_name
        dest_repo, tenant = parse_storage_repository_name(repository)
        image = migrate_disk_image(image, dest_repo)
        info("%s has been successfully migrated as %s." % (old_qualified_name, image.qualified_name))


class ResizeDiskImage(Command):
    """Resize a virtual disk image."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="The repository-qualified name (repo/image) of the disk image to resize.")

    def execute(self, name, **kwargs):
        image = parse_disk_image_name(name)
        if image is None:
            info("Disk image '%s' does not exist." % name)
            return
        if len(image.block_devices) > 0:
            warn("Refusing to resize an online disk image")
            return
        image_qualified_name = image.qualified_name
        delete_disk_image(image)
        info("%s has been deleted." % image_qualified_name)


class DeleteDiskImage(Command):
    """Delete a virtual disk image."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="The repository-qualified name (repo/image) of the disk image to delete.")

    def execute(self, name, **kwargs):
        image = parse_disk_image_name(name)
        if image is None:
            info("Disk image '%s' does not exist." % name)
            return
        if len(image.block_devices) > 0:
            warn("Refusing to delete a referenced disk image.", "'%s' is currently bound as:" % image.qualified_name)
            for device in image.block_devices:
                _print("\t%s" % device.qualified_name, file=sys.stderr)
            return
        image_qualified_name = image.qualified_name
        delete_disk_image(image)
        info("%s has been deleted." % image_qualified_name)


class ListDiskImages(Command):
    __doc__ = textwrap.dedent("""\
    List all virtual disk images, optionally limiting
    the list to a repository or tenant.""")

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument('scope', nargs=argparse.OPTIONAL,
                            help="A tenant name or tenant-qualified repository name.")

    def execute(self, scope=None, **kwargs):
        group_by = None
        disk_images = []
        if scope:
            try:
                repository, _ = parse_storage_repository_name(scope)
                disk_images.extend(repository.disk_images)
            except:
                # assume tenant name was supplied instead of a repository name
                repositories = get_storage_repositories_for_tenant(parse_tenant_name(scope))
                for repository in repositories:
                    disk_images.extend(repository.disk_images)
                group_by = DisplayField(lambda r: "%s images" % r, lambda i: i.repository.name)
        else:
            # list em all grouped by repo
            for repository in get_storage_repositories():
                disk_images.extend(repository.disk_images)
            group_by = DisplayField(lambda r: "%s images" % r, lambda i: i.repository.name)

        display_table(
            disk_images, [
                DisplayField("Name", lambda i: i.qualified_name),
                DisplayField("Parent", lambda i: i.parent.name if i.parent else ''),
                DisplayField("Created", lambda i: i.created),
                DisplayField("Mode", lambda i: 'r' if i.read_only else 'w'),
                DisplayField("Present", lambda i: 'X' if i.present else ''),
                DisplayField("Locked", lambda i: 'X' if i.locked else ''),
                DisplayField("Shared", lambda i: 'X' if i.shareable else ''),
                DisplayField("Logical Size", lambda i: si_units(i.virtual_size), justify='r'),
                DisplayField("Physical Size", lambda i: si_units(i.physical_size), justify='r'),
            ],
            group_by=group_by,
            order_by=lambda i: i.name
        )


class AddRepository(Command):
    __doc__ = textwrap.dedent("""\
    Import an existing physical block device as a disk image
    storage repository.
    """)

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-t", "--tenant", help="The owning tenant.")
        parser.add_argument('name', help='''
            A short name for the repository.
        ''')
        parser.add_argument('path', help='''
            The complete physical device path (e.g. /dev/vg1).
        ''')
        parser.add_argument('-d', '--driver', metavar='DRIVER',  default="lvm2", choices=["lvm2"],
                            help='repository driver type (default: lvm2)')
        parser.add_argument(
            '--force', action='store_true', default=False, help=textwrap.dedent("""\
                Ah, push it - push it good\n
                Ah, push it - p-push it real good
            """)
        )

    def execute(self, **kwargs):
        tenant = parse_tenant_name(kwargs.get("tenant"))
        add_storage_repository(name=kwargs.get("name"), impl=kwargs.get("driver"),
                               path=kwargs.get("path"), tenant=tenant)


class ConnectRepository(Command):
    """Connect an existing storage repository to a cluster."""
    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument('repo', help="The tenant-qualified name of the repository to connect.")
        parser.add_argument('cluster', nargs=argparse.OPTIONAL,
                            help="The cluster to connect to. "
                                 "Defaults to the cluster to which the owning tenant is currently bound.")

    def execute(self, repo, cluster, **kwargs):
        repo, tenant = parse_storage_repository_name(repo)
        if cluster is None:
            if tenant:
                if tenant.cluster:
                    cluster = tenant.cluster
                else:
                    raise XenmasterError("Unable to connect '%s' to '%s's cluster." % (repo.name, tenant.shortname),
                                         "'%s' is not currently bound to any cluster." % tenant.shortname)
            else:
                raise XenmasterError("Unable to connect '%s'" % repo.name,
                                     "The 'cluster' argument must be specified when "
                                     "connecting a global storage repository.")
        else:
            cluster = get_cluster_with_name(cluster)
        connect_storage_repository(repo, cluster)


class CreateRepository(Command):
    """Create a new disk image storage repository."""
    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("-t", "--tenant", help="The owning tenant.")
        parser.add_argument('-c', '--cluster', metavar='CLUSTER', help="The cluster to connect to. "
                            "Defaults to the cluster to which the owning tenant is currently bound.")
        parser.add_argument('-H', '--host', metavar='HOST', help="The host on which the repository should be created.")
        parser.add_argument('-d', '--driver', metavar='DRIVER',  default="lvm2", choices=["lvm2"],
                            help='repository driver type (default: lvm2)')
        parser.add_argument('name', help="A short name for the repository.")
        parser.add_argument('capacity', help=textwrap.dedent("""\
            The virtual capacity of this repository in MB.
            The physical storage committed may be less than this value if the driver supports thin provisioning.
        """))
        driver_params = parser.add_argument_group("key-value arguments", description="driver-specific parameters as key=value pairs")
        driver_params.add_argument("parameters", nargs=argparse.ZERO_OR_MORE, metavar="key=value",
                                   help="A key=value tuple which will be passed through to the storage driver.")

    def execute(self, name, capacity, driver=None, tenant=None, cluster=None, host=None, parameters=None, **kwargs):
        _tenant = parse_tenant_name(tenant)
        if cluster is None:
            if tenant == get_global_tenant():
                raise XenmasterError("Unable to create '%s'" % name,
                                     "The 'cluster' argument must be specified when "
                                     "creating a 'global' storage repository.")
            else:
                if _tenant.cluster:
                    _cluster = _tenant.cluster
                else:
                    raise XenmasterError("Unable to create '%s' on '%s's cluster." % (name, _tenant.shortname),
                                         "'%s' is not currently bound to any cluster." % _tenant.shortname)
        else:
            _cluster = get_cluster_with_name(cluster)
        driver_params = {}
        if parameters:
            for tup in parameters:
                key, value = tup.split('=', 1)
                if value:
                    driver_params[key] = value
        create_storage_repository(name=name, impl=driver, capacity=capacity, tenant=_tenant, 
                                  cluster=_cluster, driver_parameters=driver_params)


class ListRepositories(Command):
    __doc__ = textwrap.dedent("""\
    List disk image storage repositories defined for
    the named tenant (or all repositories by tenant).""")

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("tenant", nargs="?", help="The name of a tenant for which to list repositories.")

    @staticmethod
    def _alloc_percentage(repo, value):
        return (100.0 * value / repo.allocation) if repo.allocation else 0

    def execute(self, tenant=None, **kwargs):
        if tenant:
            tenant = parse_tenant_name(tenant)
            repos = get_storage_repositories_for_tenant(tenant)
        else:
            repos = get_storage_repositories()

        display_table(
            repos, [
                DisplayField("Name", lambda r: r.name),
                DisplayField("Type", lambda r: r.impl),
                DisplayField("Description", lambda r: r.description or ""),
                DisplayField("Images", lambda r: len(r.disk_images), justify='r'),
                DisplayField("Allocation", lambda r: si_units(r.allocation), justify='r'),
                DisplayField("Used", lambda r: si_units(r.utilisation), justify='r'),
                DisplayField("Free", lambda r: "%s (%5.1f%%)" % (si_units(r.free), self._alloc_percentage(r, r.free)),
                             justify='r'),
            ],
            group_by=DisplayField(lambda t: "%s storage repositories" % t, lambda r: r.tenant.shortname),
            order_by=lambda r: r.name
        )


meta = AttributedDict({
    "help": __doc__,
    "actions": [
        ("create-repo", CreateRepository),
        ("add-repo", AddRepository),
        ("connect-repo", ConnectRepository),
        ("list-repos", ListRepositories),
        ("create-image", CreateDiskImage),
        ("import-image", ImportDiskImage),
        ("copy-image", CopyDiskImage),
        ("migrate-image", MigrateDiskImage),
        ("delete-image", DeleteDiskImage),
        ("list-images", ListDiskImages),
    ]
})
