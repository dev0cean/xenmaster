import textwrap

from ocsutil.console import Command

from libxm import model
from libxm.model.guest import HVM, VNCConfig
from libxm.model.storage import BlockDeviceSpec, BlockDeviceType
from libxm.model.network import GuestInterfaceDriver
from libxm.service.storage_service import add_storage_repository
from libxm.service.guest_service import create_os, create_guest_template, get_tenant_with_name, create_tenant
from ocsutil import AttributedDict


class Initialize(Command):
    """Initialize the persistent store and preseed it with some OS and Template defaults."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("--really", required=True, action="store_true", default=False,
                            help="Accident safety catch. It happens.")
        parser.add_argument("--db", help=textwrap.dedent("""\
        Alternate database URL in the form <driver_scheme>://<user>:<pass>@<host>/<schema>
        e.g. mysql+mysqlconnector://xensei:x3nm4573r@localhost/xensei
        (default: the value of db.url in your xenmaster config file).
        """))

    def execute(self, **kwargs):
        model.recreate_schema()

        global_tenant = get_tenant_with_name(None)
        if global_tenant is None:
            global_tenant = create_tenant(None, "global")

        # os
        create_os("BIG-IP", "10.2.2", "hvm")
        create_os("RHEL", "6.4", "linux")
        create_os("Debian", "Wheezy", "linux")
        create_os("Debian", "Jessie", "linux")
        create_os("Windows", "2003_Standard", "hvm")
        create_os("Windows", "2003_R2_Standard_SP2", "hvm")
        create_os("Windows", "2008_Standard", "hvm")

        # templates
        create_guest_template(
            "bigip",
            tenant=global_tenant,
            os_name="BIG-IP",
            os_release="10.2.2",
            vmm=HVM(arch="x86_32", vcpus=2, memory=2048, shadow_memory=18, enable_viridian=False,
                    enable_nested_hvm=True, enable_xen_platform_pci=False),
            block_devices=[
                BlockDeviceSpec("hda", BlockDeviceType.hdd, False, 20480)
            ],
            nics=[
                GuestInterfaceDriver.rtl8139,
                GuestInterfaceDriver.rtl8139,
            ],
            vnc_config=VNCConfig(vnc_enabled=True, vnc_password="d3v734m"),
            cpus="2-15",
            cpu_weight=512,
            stdvga=False
        )
        create_guest_template(
            "win2k3",
            tenant=global_tenant,
            os_name="Windows",
            os_release="2003_Standard",
            vmm=HVM(arch="x86_32", memory=2048, enable_viridian=True, enable_nested_hvm=True),
            block_devices=[
                BlockDeviceSpec("hda", BlockDeviceType.hdd, False, 20480)
            ],
            nics=[GuestInterfaceDriver.rtl8139],
            vnc_config=VNCConfig(vnc_enabled=True, vnc_password="d3v734m")
        )
        create_guest_template(
            "win2k3r2",
            tenant=global_tenant,
            os_name="Windows",
            os_release="2003_R2_Standard_SP2",
            vmm=HVM(arch="x86_64", memory=2048, enable_viridian=True, enable_nested_hvm=True),
            block_devices=[
                BlockDeviceSpec("hda", BlockDeviceType.values.hdd, False, 20480)
            ],
            nics=[GuestInterfaceDriver.rtl8139],
            vnc_config=VNCConfig(vnc_enabled=True, vnc_password="d3v734m")
        )
        create_guest_template(
            "win2k8",
            tenant=global_tenant,
            os_name="Windows",
            os_release="2008_Standard",
            vmm=HVM(arch="x86_64", memory=2048, enable_viridian=True, enable_nested_hvm=True),
            block_devices=[
                BlockDeviceSpec("hda", BlockDeviceType.hdd, False, 20480)
            ],
            nics=[GuestInterfaceDriver.rtl8139],
            vnc_config=VNCConfig(vnc_enabled=True, vnc_password="d3v734m")
        )
        create_guest_template(
            "rhel64",
            tenant=global_tenant,
            os_name="RHEL",
            os_release="6.4",
            vmm=HVM(arch="x86_64", memory=1024),
            block_devices=[
                BlockDeviceSpec("xvda", BlockDeviceType.hdd, False, 4096),
                BlockDeviceSpec("xvdb", BlockDeviceType.hdd, False, 2048)
            ],
            nics=[GuestInterfaceDriver.netfront],
            vnc_config=VNCConfig(vnc_enabled=True, vnc_password="d3v734m")
        )
        create_guest_template(
            "wheezy",
            tenant=global_tenant,
            os_name="Debian",
            os_release="Wheezy",
            vmm=HVM(arch="x86_64", memory=1024),
            block_devices=[
                BlockDeviceSpec("xvda", BlockDeviceType.hdd, False, 4096),
                BlockDeviceSpec("xvdb", BlockDeviceType.hdd, False, 2048)
            ],
            nics=[GuestInterfaceDriver.netfront],
            vnc_config=VNCConfig(vnc_enabled=True, vnc_password="d3v734m")
        )
        create_guest_template(
            "jessie",
            tenant=global_tenant,
            os_name="Debian",
            os_release="Jessie",
            vmm=HVM(arch="x86_64", memory=1024),
            block_devices=[
                BlockDeviceSpec("xvda", BlockDeviceType.hdd, False, 4096),
                BlockDeviceSpec("xvdb", BlockDeviceType.hdd, False, 2048)
            ],
            nics=[GuestInterfaceDriver.netfront],
            vnc_config=VNCConfig(vnc_enabled=True, vnc_password="d3v734m")
        )

        # storage
        #add_storage_repository("media", "lvm2", "/dev/media")

        # roles
        # default_role = create_role("default")
        # default_role.packages["rhel", ""] = [
        #     "autofs",
        #     "nfs-utils",
        #     "ethtool",
        #     "dhclient",
        #     "tcpdump",
        #     "traceroute",
        #     "openssh-server",
        #     "openssh",
        #     "iptables",
        #     "openssh-clients",
        #     "yum",
        #     "wget",
        #     "vim-enhanced",
        #     "vim-common",
        #     "bind-utils",
        #     "ntp",
        #     "expect",
        #     "man",
        #     "man-pages",
        #     "bzip2",
        #     "openldap-clients",
        #     "vixie-cron",
        #     "wotifgroup-iam-test",
        #     "ruby",
        #     "ruby-rdoc",
        #     "ruby-libs",
        #     "ruby-irb",
        #     "rubygems"
        # ]
        # default_role.packages["rhel", "5.7"] = [
        #     "openldap-clients",
        #     "vixie-cron",
        # ]
        # default_role.packages["rhel", "6.2"] = [
        #     "telnet",
        # ]


meta = AttributedDict({
    "help": __doc__,
    "actions": [
        ("init", Initialize),
    ]
})
