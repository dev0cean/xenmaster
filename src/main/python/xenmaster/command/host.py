import re
from ocsutil import AttributedDict, si_units
from ocsutil.console import Command, display_table, DisplayField

from libxm.model.host import Host
from libxm.service.host_service import get_cluster_with_name, sync_cluster_state


def _display_host_info(host):
    """
    :type host: Host
    """
    class vm_attr(object):
        def __init__(self, label, value, option=None):
            super(vm_attr, self).__init__()
            self.label = label
            self.value = value
            self.option = option

    display_table(
        [
            vm_attr("Name", host.qualified_name),
            vm_attr("UUID", host.uuid),
            vm_attr("State", host.state),
            vm_attr("Architecture", host.arch),
            vm_attr("CPU Cores", host.vcpus),
            vm_attr("Memory", si_units(host.memory)),
        ], [
            DisplayField("Property", lambda a: a.label),
            DisplayField("Value", lambda a: a.value),
        ], group_by=DisplayField("Basic VM Parameters", None), group_headings=True,
    )


class Start(Command):
    def __init__(self):
        pass

    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        pass


class Stop(Command):
    def __init__(self):
        pass

    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        pass


class Restart(Command):
    def __init__(self):
        pass

    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        pass


class Vacate(Command):
    def __init__(self):
        pass

    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        pass


class List(Command):
    """List host stats for a cluster."""

    def __init__(self):
        pass

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("cluster", help="The cluster to query.")

    @staticmethod
    def host_name_ascending(host):
        if host is None:
            return None
        match = re.search(r'(.*)(\d)+$', host.name)
        if match:
            return match.group(1), int(match.group(2))
        else:
            return host.name, None

    def execute(self, cluster, **kwargs):
        _cluster = get_cluster_with_name(cluster)
        assert bool(_cluster), "There is no registered cluster with name '%s'" % cluster
        sync_cluster_state(_cluster)

        display_table(
            _cluster.hosts, [
                DisplayField("Name", lambda h: h.name),
                DisplayField("UUID", lambda h: h.uuid),
                DisplayField("Status", lambda h: h.status),
                DisplayField("Arch", lambda h: h.arch),
                DisplayField("Cores", lambda h: h.cpus, justify='r'),
                DisplayField("Cores Committed", lambda h: h.committed_cpus, justify='r'),
                DisplayField("Cores Free", lambda h: h.uncommitted_cpus, justify='r'),
                DisplayField("Memory", lambda h: si_units(h.memory), justify='r'),
                DisplayField("Memory Committed", lambda h: si_units(h.committed_memory), justify='r'),
                DisplayField("Memory Free", lambda h: si_units(h.free_memory), justify='r'),
            ],
            order_by=lambda h: h.index, group_headings=False
        )


meta = AttributedDict({
    "help": __doc__,
    "actions": [
        ("start", Start),
        ("stop", Stop),
        ("restart", Restart),
        ("vacate", Vacate),
        ("list", List),
    ]
})
