import re
from ocsutil import AttributedDict, si_units
from ocsutil.console import display_table, Command, DisplayField

from libxm.exceptions import XenmasterError
from libxm.model.host import Host
from libxm.service.host_service import get_cluster_with_name, sync_cluster_state, create_cluster, \
	add_host, destroy_cluster
from libxm.service.storage_service import parse_storage_repository_name, connect_storage_repository, \
    set_cluster_default_storage_repository


def _display_cluster_info(host):
    """
    :type host: Host
    """
    class Attr(object):
        def __init__(self, label, value, option=None):
            super(Attr, self).__init__()
            self.label = label
            self.value = value
            self.option = option

    display_table(
        [
            Attr("Name", host.name),
            Attr("UUID", host.uuid),
            Attr("State", host.state),
            Attr("Architecture", host.arch),
            Attr("CPU Cores", host.vcpus),
            Attr("Memory", si_units(host.memory)),
        ], [
            DisplayField("Property", lambda a: a.label),
            DisplayField("Value", lambda a: a.value),
        ], group_by=DisplayField("Basic VM Parameters", None), group_headings=True,
    )


class Add(Command):
    """Add a new cluster."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="A short name to identify the cluster.")

    def execute(self, name, **kwargs):
        from libxm import config
        if get_cluster_with_name(name) is not None:
            raise XenmasterError("A cluster already exists with name '%s'." % name)
        cluster = create_cluster(name)
        master_name = config.get(name, "master")
        add_host(master_name, master_name, cluster, 0)
        sync_cluster_state(cluster)


class Remove(Command):
    """ Remove a cluster """

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="The cluster to remove.")

    def execute(self, name, **kwargs):
        destroy_cluster(name)


class Restart(Command):

    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        pass


class Vacate(Command):

    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        pass


class List(Command):
    """List cluster stats."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("cluster", help="The cluster to query.")

    @staticmethod
    def host_name_ascending(host):
        if host is None:
            return None
        match = re.search(r'(.*)(\d)+$', host.name)
        if match:
            return match.group(1), int(match.group(2))
        else:
            return host.name, None

    def execute(self, cluster, **kwargs):
        _cluster = get_cluster_with_name(cluster)
        assert bool(_cluster), "There is no registered cluster with name '%s'" % cluster
        sync_cluster_state(_cluster)

        display_table(
            _cluster.hosts, [
                DisplayField("Name", lambda h: h.name),
                DisplayField("UUID", lambda h: h.uuid),
                DisplayField("Status", lambda h: h.status),
                DisplayField("Arch", lambda h: h.arch),
                DisplayField("Cores", lambda h: h.cpus, justify='r'),
                DisplayField("Cores Committed", lambda h: h.committed_cpus, justify='r'),
                DisplayField("Cores Free", lambda h: h.uncommitted_cpus, justify='r'),
                DisplayField("Memory", lambda h: si_units(h.memory), justify='r'),
                DisplayField("Memory Committed", lambda h: si_units(h.committed_memory), justify='r'),
                DisplayField("Memory Free", lambda h: si_units(h.free_memory), justify='r'),
            ],
            order_by=lambda h: h.index, group_headings=False
        )


class SetMaster(Command):
    """Set the master for a cluster."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("cluster", help="The short name of the cluster to modify.")
        parser.add_argument('master', help="The unqualified name of an existing cluster member.")

    def execute(self, name, repo, **kwargs):
        cluster = get_cluster_with_name(name)
        assert bool(cluster), "There is no registered cluster with name '%s'" % name
        sync_cluster_state(cluster)
        _repo, tenant = parse_storage_repository_name(repo)
        connect_storage_repository(_repo, cluster)


class SetDefaultStorageRepository(Command):
    """Set the default storage repository for a cluster."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="The short name of the cluster to modify.")
        parser.add_argument('repo', help="The tenant-qualified name of the repository to connect.")

    def execute(self, name, repo, **kwargs):
        cluster = get_cluster_with_name(name)
        assert bool(cluster), "There is no registered cluster with name '%s'" % name
        sync_cluster_state(cluster)
        _repo, tenant = parse_storage_repository_name(repo)
        set_cluster_default_storage_repository(cluster, _repo)
        connect_storage_repository(_repo, cluster)


meta = AttributedDict({
    "help": __doc__,
    "actions": [
        ("add", Add),
        ("remove", Remove),
        ("list", List),
        ("set-master", SetMaster),
        ("set-default-repo", SetDefaultStorageRepository),
    ]
})
