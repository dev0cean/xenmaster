import textwrap
from ocsutil import AttributedDict
from ocsutil.console import Command, info, display_table, DisplayField

from libxm.exceptions import XenmasterError
from libxm.service.host_service import get_cluster_with_name
from libxm.service.guest_service import create_tenant, get_tenant_with_name, bind_tenant, get_tenants
from libxm.service.storage_service import get_storage_repository_with_name, set_tenant_default_storage_repository


class Create(Command):
    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("key",
                            help=textwrap.dedent("""\
                            The short string that will be appended to unqualified resource names to uniquely
                            identify them for this tenant.
                            """))
        parser.add_argument("cluster",
                            help=textwrap.dedent("""\
                            The name of the host cluster to which the tenant will be bound.
                            """))
        parser.add_argument("shortname", nargs="?",
                            help=textwrap.dedent("""\
                            The short string that will be used when displaying the tenant attributes
                            (defaults to the value of the 'key' parameter).
                            """))
        parser.add_argument("longname", nargs="?",
                            help=textwrap.dedent("""\
                            An optional full name for display purposes.
                            """))

    def execute(self, key, cluster, shortname=None, longname=None, **kwargs):
        _cluster = get_cluster_with_name(cluster)
        tenant = create_tenant(key, shortname, _cluster, longname)
        info("A new tenant '%s' has been created on cluster '%s'." % (tenant.shortname, _cluster.name))


# set default cluster?
class BindToCluster(Command):
    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name",
                            help=textwrap.dedent("""\
                            The tenant to bind.
                            """))
        parser.add_argument("cluster",
                            help=textwrap.dedent("""\
                            The name of the host cluster to which the tenant will be bound.
                            """))

    def execute(self, name, cluster, **kwargs):
        tenant = get_tenant_with_name(name)
        if tenant is None:
            raise XenmasterError("No tenant exists with name '%$'", name)
        _cluster = get_cluster_with_name(cluster)
        if _cluster is None:
            raise XenmasterError("No cluster exists with name '%$'", cluster)
        bind_tenant(tenant, _cluster)
        info("Tenant '%s' has been bound to cluster '%s'." % (tenant.shortname, _cluster.name))


class Start(Command):
    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        raise NotImplementedError()


class Stop(Command):
    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        raise NotImplementedError()


class Restart(Command):
    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        raise NotImplementedError()


class Pause(Command):
    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        raise NotImplementedError()


class Resume(Command):
    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        raise NotImplementedError()


class Migrate(Command):
    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        raise NotImplementedError()


class Destroy(Command):
    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        raise NotImplementedError()


class List(Command):
    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("cluster", nargs="?",
                            help=textwrap.dedent("""\
                            The name of the host cluster to which the tenant will be bound.
                            """))

    def execute(self, cluster=None, **kwargs):
        if cluster:
            _cluster = get_cluster_with_name(cluster)
            tenants = _cluster.tenants
            if _cluster is None:
                raise XenmasterError("There is no '%s' cluster defined" % cluster)
        else:
            tenants = get_tenants()
        display_table(
            tenants, [
                DisplayField("Name", lambda t: t.shortname),
                DisplayField("Guests", lambda t: len(t.guests), justify="right"),
            ],
            group_by=DisplayField(lambda c: "%s tenants" % c, lambda t: t.cluster.name if t.cluster else "unbound"),
            group_headings=False,
            order_by=lambda t: t.name
        )


class AddNetwork(Command):
    @classmethod
    def configure_parser(cls, parser):
        pass

    def execute(self, *args, **kwargs):
        raise NotImplementedError()


class SetDefaultStorageRepository(Command):
    """Set the default storage repository for a tenant."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("tenant", help="The short name of the tenant to update.")
        parser.add_argument('repo', help="The unqualified name of a tenant-local or global repository.")

    def execute(self, tenant, repo, **kwargs):
        _tenant = get_tenant_with_name(tenant)
        _repo = get_storage_repository_with_name(repo, _tenant)
        set_tenant_default_storage_repository(_tenant, _repo)


meta = AttributedDict({
    "help": __doc__,
    "actions": [
        ("create", Create),
        ("start", Start),
        ("stop", Stop),
        ("restart", Restart),
        ("pause", Pause),
        ("resume", Resume),
        ("migrate", Migrate),
        ("destroy", Destroy),
        ("list", List),
        # networking
        ("add-network", AddNetwork),
        # ("connect-nic", ConnectGuestInterface),
        ("bind-to-cluster", BindToCluster),
        #("set-default-repo", SetDefaultStorageRepository),
    ]
})
