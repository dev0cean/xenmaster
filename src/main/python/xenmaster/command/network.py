import itertools
import textwrap
from ocsutil import AttributedDict
from ocsutil.console import info, display_table, DisplayField, Command

from libxm.exceptions import XenmasterError
from libxm.service.host_service import get_host_with_name, get_cluster_with_name
from libxm.service.guest_service import get_tenants, get_global_tenant, parse_tenant_name
from libxm.service.network_service import create_network, create_vlan, get_network_with_name, get_networks, \
    parse_network_name_hard, undeploy_network, deploy_network


class CreateNetworkCommand(Command):
    """Create a new network."""
    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("name", help="A tenant-qualified (tenant:network) name for the network.")

    def execute(self, name, **kwargs):
        tenant_name, _, network_name = name.rpartition(':')
        tenant = parse_tenant_name(tenant_name)
        network = create_network(network_name, tenant)
        info("Network '%s' created." % network.qualified_name)


class RedeploySwitchCommand(Command):
    """Force redeployment of a network switch."""
    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("network", help="The tenant-qualified (tenant:network) name of the network.")
        parser.add_argument("host", help="The cluster-qualified (host.cluster) name of the host.")

    def execute(self, network, host, **kwargs):
        tenant_name, _, network_name = network.rpartition(':')
        host_name, _, cluster_name = host.partition('.')
        tenant = parse_tenant_name(tenant_name)
        if cluster_name:
            _host = get_host_with_name(host_name, get_cluster_with_name(cluster_name))
        else:
            _host = get_host_with_name(host_name, tenant.cluster)
        _network = get_network_with_name(network_name, tenant)
        undeploy_network(_network, _host)
        deploy_network(_network, _host)


class ListNetworks(Command):
    """List the networks of the named tenant (or all networks by tenant)."""
    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("tenant", nargs="?", help="The name of a tenant for which to list networks.")

    def execute(self, tenant=None, **kwargs):
        if tenant:
            _tenant = parse_tenant_name(tenant)
            networks = _tenant.networks
            if not networks:
                print("There are currently no '%s' networks defined." % _tenant.shortname)
                return
        else:
            networks = list(itertools.chain(*(t.networks for t in get_tenants())))
        display_table(
            networks, [
                DisplayField("Name", lambda n: n.qualified_name),
                DisplayField("MTU", lambda n: n.mtu, justify="right"),
                DisplayField("Flow Controller", lambda n: n.controller or ''),
                DisplayField("Deployed Host Segments",
                             lambda n: ','.join(h.name for h in sorted(n.hosts, key=lambda h: h.index))),
            ],
            group_by=DisplayField(lambda t: "%s networks" % t, lambda n: n.tenant.shortname),
            group_headings=False,
            order_by=lambda network: network.name
        )


class CreateVLANCommand(Command):
    """Create a VLAN on a tenant network."""
    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("network", help="The tenant-qualified (tenant:network) name of the network "
                                            "on which the vlan will be created.")
        parser.add_argument("tag", type=int, choices=range(1, 4096), metavar="1..4095",
                            help="The desired tag for the vlan, unique for the named network.")
        parser.add_argument("name", nargs="?", help="An optional name for the vlan, unique for the named network.")

    def execute(self, network, tag, name=None, **kwargs):
        _network, _ = parse_network_name_hard(network)
        vlan = create_vlan(_network, tag, name)
        info("VLAN '%s' created on the '%s' network" % (vlan, network))


class ListNetworkVLANs(Command):
    """List the VLANs defined for the named tenant or network."""

    @classmethod
    def configure_parser(cls, parser):
        parser.add_argument("scope", metavar="{tenant|network}", help=textwrap.dedent("""\
            The tenant-qualified (tenant:network) name of the network
            for which vlans will be listed."""))

    def execute(self, scope, **kwargs):
        prefix, sep, suffix = scope.partition(':')
        if sep:
            tenant = parse_tenant_name(prefix)
            network = get_network_with_name(suffix, tenant)
            if network:
                vlans = network.vlans.values()
                if not vlans:
                    print("There are currently no vlans defined on the '%s' network." % network.qualified_name)
                    return
            else:
                tenant_networks = get_networks(tenant)
                if tenant_networks:
                    error_detail = "Existing '%s' networks are:\n\t%s" % (
                        tenant.shortname, "\n\t".join(sorted(n.name for n in tenant_networks)))
                else:
                    error_detail = "There are currently no '%s' networks defined." % tenant.shortname
                raise XenmasterError("The '%s' tenant has no network with name '%s'."
                                     % (tenant.shortname, suffix), error_detail)
        else:
            # try tenants first
            try:
                tenant = parse_tenant_name(prefix)
                vlans = list(itertools.chain(*(n.vlans.values() for n in tenant.networks)))
                if not vlans:
                    print("There are currently no vlans defined on any '%s' network." % tenant.shortname)
                    return
            except XenmasterError:
                global_tenant = get_global_tenant()
                network = get_network_with_name(prefix, global_tenant)
                if network:
                    vlans = network.vlans.values()
                    if not vlans:
                        print("There are currently no vlans defined on the '%s' network." % network.qualified_name)
                        return
                else:
                    raise XenmasterError("No tenant or 'global' network exists with name '%s'." % scope)

        display_table(
            vlans, [
                DisplayField("Name", lambda v: v.name),
                DisplayField("Tag", lambda v: v.tag),
            ],
            group_by=DisplayField(lambda n: "%s VLANs" % n, lambda v: v.network.name),
            order_by=lambda v: v.tag
        )


meta = AttributedDict({
    "help": __doc__,
    "actions": [
        ("create", CreateNetworkCommand),
        ("list", ListNetworks),
        ("create-vlan", CreateVLANCommand),
        ("list-vlans", ListNetworkVLANs),
        ("redeploy-switch", RedeploySwitchCommand),
    ]
})
