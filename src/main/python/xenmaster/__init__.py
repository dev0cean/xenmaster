import os
import libxm
import sys

__all__ = [
    "init"
]

__version__ = "2.5.1"


def init(args):
    config_root = os.path.join("/" if sys.prefix == "/usr" else sys.prefix, "etc/xenmaster")
    args.setdefault("config", [os.path.join(config_root, "xm.conf"), os.path.expanduser("~/.config/xenmaster/xm.conf")])
    libxm.init(args)
    args.pop("config")
