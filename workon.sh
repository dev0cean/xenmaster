#!/bin/bash
export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh || {
    echo "This tool requires the python virtualenvwrapper package"
    exit 1
}
workon xenmaster
