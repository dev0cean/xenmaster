#!/bin/bash

colour_red='\033[1;31m'
colour_green='\033[0;32m'
colour_grey='\033[0;37m'
colour_off='\033[00m'
: ${verbose:="true"}
: ${realm:="HIVE"}
export realm
export realm_domain=$(tr A-Z a-z <<<$realm)
: ${root_domain:=$realm_domain}
export root_domain
export realm_ldap_base="dc=${realm_domain//./',dc='}"
export KRB5CCNAME=$(mktemp -p /tmp krb5cc_$(id -u).XXXXXXXX)
export minPersonUidNumber=2000
export minServiceUidNumber=1000
export minGidNumber=1000
kadmin="/usr/bin/kadmin -r $realm"


function report_success {
	echo -e "${colour_green}$@${colour_off}"
}

function report_error {
	echo -e "${colour_red}$@${colour_off}" >&2
}

function report_info {
	echo -e "${colour_grey}$@${colour_off}"
}

function debug {
	[ ${verbose} ] && report_info "$@" >&2
}

function userIsAuthenticated {
	/usr/bin/klist 2> /dev/null | grep -q -E "Default principal: .*@$realm"
}

function userIsAdmin {
	/usr/bin/klist 2> /dev/null | grep -q -E "Default principal: .*/admin@$realm"
}

function requireAuthentication {
	# default duration is 3 minutes
	# valid duration strings are as per /usr/bin/kinit -l
	local duration=${1:-"3m"}
	if ! userIsAuthenticated ; then
		attempt=1
		[ $(id -u) -eq 0 ] && as="root/admin@$realm"
		until userIsAuthenticated
		do
			/usr/bin/kinit -l $duration $as && continue
			if (( ++attempt > 3 )); then
				echo "Too many failed attempts. Exiting."
				exit 1
			else
				read -n 1 -p "Try again? [Y/n]: " retry; echo
				case "$retry" in n|N) exit 1;; esac
			fi
		done
	fi
}

function requireAdmin {
	# default duration is 3 minutes
	# valid duration strings are as per /usr/bin/kinit -l
	local duration=${1:-"3m"}
	# obtain an admin ticket if one is not already in the credentials cache
	if ! userIsAdmin ; then
		echo "Only admin users may administer the realm."
		attempt=1
		local username=$(id -un)
		until userIsAdmin
		do
			/usr/bin/kinit -l $duration $username/admin@$realm && continue
			if (( ++attempt > 3 )); then
				echo "Too many failed attempts. Exiting."
				exit 1
			else
				read -n 1 -p "Try again? [Y/n]: " retry; echo
				case "$retry" in n|N) exit 1;; esac
			fi
		done
	fi
	if ! (/usr/bin/klist 2> /dev/null | grep -q "kadmin/admin@$realm"); then
		/usr/bin/kvno kadmin/admin@$realm -q || {
			relinquishAdmin
			report_error "Unable to add kadmin/admin@$realm service ticket to your credentials cache. Bailing."
			exit 1
		}
	fi
}

function dump {
	[ $verbose ] || return
	for var in "$@"; do
		eval echo "$var = ${!var}"
	done
}

function debug_and_exit {
	for var in "$@"; do
		eval echo "$var = ${!var}"
	done
	exit 0
}

function relinquishAdmin {
	/usr/bin/kdestroy -q
}

function check_mandatory {
	local error=
	for varname in "$@"; do
		#debug "$varname: ${!varname}"
		[ ${!varname} ] || error+="in function ${FUNCNAME[1]}(${BASH_SOURCE[1]}:${BASH_LINENO[0]}): $varname is a mandatory argument\n"
	done
	if [ ${#error} -gt 0 ]; then
		report_error $error
		return 1
	fi
	return 0
}

function invocation_error {
	report_error "in function ${FUNCNAME[1]}(${BASH_SOURCE[1]}:${BASH_LINENO[0]}): $1\n"
}

function debug_entry {
	debug "${FUNCNAME[1]} $@\n"
}


###############################################
## Functions which only query the LDAP store **
###############################################

function getUidNumber {
	local uid=$1
	check_mandatory uid || return 1
	requireAuthentication
	sed -e "s/uidNumber:[[:space:]]*//" <<< $(ldapsearch -LLLQ -s one -b ou=users,$realm_ldap_base "(&(uid=$uid)(objectClass=posixAccount))" uidNumber | grep uidNumber)
}

function getNextPersonUidNumber {
	requireAuthentication
	local next=$(( $(ldapsearch -LLLQ -s one -b ou=users,$realm_ldap_base "(objectClass=person)" uidNumber | grep uidNumber | sed "s/uidNumber:[[:space:]]*//" | sort -r | head -1) + 1 ))
	if [ $next -gt $minPersonUidNumber ]; then
		echo $next
	else
		echo $minPersonUidNumber
	fi
}

function getNextServiceUidNumber {
	requireAuthentication
	local next=$(( $(ldapsearch -LLLQ -s one -b ou=users,$realm_ldap_base '(objectClass=account)' uidNumber | grep uidNumber | sed "s/uidNumber:[[:space:]]*//" | sort -r | head -1) + 1 ))
	if [ $next -gt $minServiceUidNumber ]; then
		echo $next
	else
		echo $minServiceUidNumber
	fi
}

function getGidNumber {
	local gid=$1
	check_mandatory gid || return 1
	requireAuthentication
	sed -e "s/gidNumber:[[:space:]]*//" <<< $(ldapsearch -LLLQ -s one -b ou=groups,$realm_ldap_base "(&(cn=$gid)(objectClass=posixGroup))" gidNumber | grep gidNumber)
}

function getNextGidNumber {
	requireAuthentication
	local next=$(( $(ldapsearch -LLLQ -s one -b ou=groups,$realm_ldap_base "(objectClass=posixGroup)" gidNumber | grep gidNumber | sed "s/gidNumber:[[:space:]]*//" | sort -r | head -1) + 1 ))
	if [ $next -gt $minGidNumber ]; then
		echo $next
	else
		echo $minGidNumber
	fi
}

function userExists {
	local uid=$1
	check_mandatory uid || return 1
	requireAuthentication
	if grep -q $uid <<< $( ldapsearch -LLLQ -s one -b ou=users,$realm_ldap_base uid=$uid objectClass=posixAccount ); then
		debug "User \"$uid\" exists"
		return 0
	else
		debug "User \"$uid\" does not exist"
		return 1
	fi
}

function groupExists {
	local group=$1
	check_mandatory group || return 1
	requireAuthentication
	if grep -q $group <<< $( ldapsearch -LLLQ -s one -b ou=groups,$realm_ldap_base cn=$group objectClass=posixGroup ); then
		debug "Group \"$group\" exists"
		return 0
	else
		debug "Group \"$group\" does not exist"
		return 1
	fi
}

function serviceExists {
	local service=$1
	check_mandatory service || return 1
	requireAuthentication
	if grep -q $service <<< $( ldapsearch -LLLQ -s one -b ou=services,$realm_ldap_base krbPrincipalName=$service objectClass=krbPrincipal ); then
		debug "Service \"$service\" exists"
		return 0
	else
		debug "Service \"$service\" does not exist"
		return 1
	fi
}

###########################################
## Functions which modify the LDAP store ##
###########################################

function createGroup {
	local group=$1
	check_mandatory group || return 1
	requireAdmin
	report_info "Creating the \"$group\" group"
	ldapadd -Q <<-EOF
		dn: cn=$group,ou=groups,$realm_ldap_base
		objectClass: posixGroup
		objectClass: top
		cn: $group
		gidNumber: $(getNextGidNumber)
	EOF
	[ $? ] || {
		report_error "Could not create the \"$group\" group"
		return 1
	}
}

function addMemberUidToGroup {
	local uid=$1
	local group=$2
	check_mandatory uid group || return 1
	requireAdmin
	report_info "Adding user \"$uid\" to the \"$group\" group"
	if ! groupExists $group; then
		report_error "Could not add user \"$uid\" to the nonexistent \"$group\" group"
		return 1
	fi
	ldapmodify -Q <<-EOF
		dn: cn=$group,ou=groups,$realm_ldap_base
		changetype: modify
		add: memberUid
		memberUid: $uid
		-
	EOF
	[ $? ] || {
		report_error "Could not add user \"$uid\" to the \"$group\" group"
		return 1
	}
}

function createPersonUser {
	# Parse arguments
	args=$(getopt -o d:p:c: -- "$@") || return 1
	eval set -- "$args"
	while true; do
		case "$1" in
			-d) shift; homedir=${1## }; homedir=${homedir%% }; shift;;
			-p) shift; password=$1; shift;;
			-c) shift; cn=${1## }; cn=${cn%% }; shift;;
			--) shift; break;;
			*) report_error "Internal error!"; return 1 ;;
		esac
	done
	[ $# -eq 1 ] || {
		invocation_error "non-option arguments must number exactly 1. You supplied \"$@\"\n"
		return 1
	}
	uid=$1
	check_mandatory uid || return 1
	: ${cn:=$uid}
	: ${gid:="users"}
	: ${homedir:="/home/$uid"}

	if [ ${#password} -gt 0 ];then
		accountStatus="active"
		password="-pw $password"
	else
		accountStatus="inactive"
		password="-randkey"
	fi

	if userExists $uid; then
		report_error "User \"$uid\" already exists"
		return 1
	fi

	if ! groupExists $gid; then
		createGroup $gid
	fi

	report_info "Adding user \"$uid\" to the $realm realm"
	requireAdmin
	ldapadd -Q <<-EOF
		dn: uid=$uid,ou=users,$realm_ldap_base
		objectClass: inetOrgPerson
		objectClass: krbPrincipalAux
		objectClass: organizationalPerson
		objectClass: person
		objectClass: posixAccount
		objectClass: top
		objectClass: wotifGroupUser
		accountStatus: $accountStatus
		cn: $cn
		displayName: $cn
		gidNumber: $(getGidNumber $gid)
		givenName: ${cn%% *}
		homeDirectory: $homedir
		jabber: $uid@$realm_domain
		loginShell: /bin/bash
		mail: $(tr [:upper:] [:lower:] <<< ${cn// /.})@$realm_domain
		sn: ${cn##* }
		uid: $uid
		uidNumber: $(getNextPersonUidNumber)
		userPassword: {SASL}$uid@$realm
	EOF

	# only proceed if the ldapadd succeeded
	[ $? -eq 0 ] || {
		report_error "Could not create user \"$uid\" - LDAP add failed"
		return 1
	}

	$kadmin -c $KRB5CCNAME -q "ank -x dn=\"uid=$uid,ou=users,$realm_ldap_base\" $password -policy default +needchange +requires_preauth $uid" 2>&1 > /dev/null || {
		report_error "Could not create user \"$uid\" - Kerberos addprincipal failed"
		return 1
	}
}

function createDaemonUser {
	# Parse arguments
	args=$(getopt -o d:p:c: -- "$@") || return 1
	eval set -- "$args"
	while true; do
		case "$1" in
			-d) shift; homedir=${1## }; homedir=${homedir%% }; shift;;
			-p) shift; password=$1; shift;;
			-c) shift; cn=${1## }; cn=${cn%% }; shift;;
			--) shift; break;;
			*) report_error "Internal error!"; return 1 ;;
		esac
	done
	[ $# -eq 2 ] || {
		invocation_error "non-option arguments must number exactly 2. You supplied \"$@\""
		return 1
	}
	uid=$1
	gid=$2
	check_mandatory uid gid || return 1
	: ${cn:=$uid}
	: ${homedir:="/var/lib/$uid"}

	if [ ${#password} -gt 0 ];then
		password="-pw $password"
	else
		password="-randkey"
	fi

	if userExists $uid; then
		report_error "User \"$uid\" already exists"
		return 1
	fi
    if ! groupExists $gid; then
		createGroup $gid
	fi

	report_info "Adding daemon user $uid to the $realm realm"
	requireAdmin
	ldapadd -Q <<-EOF
		dn: uid=$uid,ou=users,$realm_ldap_base
		objectClass: posixAccount
		objectClass: top
		objectClass: account
		cn: $cn
		gidNumber: $(getGidNumber $gid)
		homeDirectory: $homedir
		loginShell: /sbin/nologin
		uid: $uid
		uidNumber: $(getNextServiceUidNumber)
		userPassword: {CRYPT}*
	EOF

	# only proceed if the ldapadd succeeded
	[ $? -eq 0 ] || {
		report_error "Could not create user \"$uid\" - LDAP add failed"
		return 1
	}

	$kadmin -c $KRB5CCNAME -q "ank -x dn=\"uid=$uid,ou=users,$realm_ldap_base\" $password -clearpolicy +allow_svr $uid" 2>&1 > /dev/null || {
		report_error "Could not create user \"$uid\" - Kerberos addprincipal failed"
		return 1
	}
}

function createServiceAccount {
	principal=$1
	check_mandatory principal || return 1

	requireAdmin
	if serviceExists $principal; then
		report_error "Service \"$principal\" already exists"
		return 1
	fi

	report_info "Adding service account $principal to the $realm realm"
	ldapadd -Q <<-EOF
		dn: krbPrincipalName=$principal,ou=services,$realm_ldap_base
		objectClass: krbPrincipal
		objectClass: top
		krbPrincipalName: $principal
	EOF

	# only proceed if the ldapadd succeeded
	[ $? -eq 0 ] || {
		report_error "Could not create service account \"$principal\" - LDAP add failed"
		return 1
	}

	$kadmin -c $KRB5CCNAME -q "ank -x dn=\"krbPrincipalName=$principal,ou=services,$realm_ldap_base\" -randkey -clearpolicy +allow_svr $principal" 2>&1 > /dev/null || {
		report_error "Could not create service account \"$principal\" - Kerberos addprincipal failed"
		return 1
	}
}

function getAuthoritativeForwardZones {
	requireAuthentication
	# emit the space-delimited list of zones, with more specific subdomains appearing before their parents
	ldapsearch -LLLQ -s one -b cn=forward-zones,ou=dns,$realm_ldap_base "(objectclass=dlzZone)" dlzZoneName | awk '
	END{for(j in a)printf"%s ",a[j]}/^dlzZoneName:/{x=$2;for(j=0;j<i;j++)if(index(x,z=a[j])){a[j]=x;x=z;break};a[i++]=x}'
}

function getAuthoritativeReverseZones {
	requireAuthentication
	# emit the space-delimited list of zones, with more specific subdomains appearing before their parents
	ldapsearch -LLLQ -s one -b cn=reverse-zones,ou=dns,$realm_ldap_base "(objectclass=dlzZone)" dlzZoneName | awk '
	END{for(j in a)printf"%s ",a[j]}/^dlzZoneName:/{x=$2;for(j=0;j<i;j++)if(index(x,z=a[j])){a[j]=x;x=z;break};a[i++]=x}'
}

function getEnvironmentNames {
	requireAuthentication
	# emit the space-delimited list of test-environment names
	ldapsearch -LLLQ -s one -b cn=config,ou=dhcp,$realm_ldap_base "(&(objectclass=dhcpGroup)(cn=tst*))" cn | awk '
	END{for(j in a)printf"%s ",a[j]}/^cn:/{x=$2;for(j=0;j<i;j++)if(index(x,z=a[j])){a[j]=x;x=z;break};a[i++]=x}'
}

function createDNSPTRRecord {
	local address=$1
	local hostname=$2
	check_mandatory hostname address || return 1

	requireAdmin
	inaddr=$(echo "$address" | awk -F . '{for (i=NF;i>=1;i--) printf $i"."; printf "in-addr.arpa"}')
	# iterate through the list of valid reverse zones
	# attempting to match the address prefix
	zr_name=
	for dns_zone in $(getAuthoritativeReverseZones); do
		if grep -q -E "$dns_zone$" <<<"$inaddr"; then
			zr_name=${inaddr%.$dns_zone}
			break
		fi
	done
	[ -n $zr_name ] || {
		report_error "No authoritative zone found matching address \"$inaddr\""
		return 1
	}

	record="$inaddr. PTR $hostname."
	report_info "Adding record $record"
	ldapadd -Q <<-EOF
		dn: dlzHostName=$zr_name,dlzZoneName=$dns_zone,cn=reverse-zones,ou=dns,$realm_ldap_base
		objectClass: dlzHost
		dlzHostName: $zr_name

		dn: dlzType=ptr,dlzHostName=$zr_name,dlzZoneName=$dns_zone,cn=reverse-zones,ou=dns,$realm_ldap_base
		objectClass: dlzPTRRecord
		dlzData: $hostname.
		dlzHostName: $zr_name
		dlzRecordID: 0
		dlzTTL: 3600
		dlzType: ptr
	EOF
	[ $? -eq 0 ] || {
		report_error "Could not create record \"$record\" - LDAP add failed"
		return 1
	}
}

function createDNSARecord {
	local hostname=$1
	local address=$2
	check_mandatory hostname address || return 1

	requireAdmin
	# iterate through the list of valid forward zones
	# attempting to match the hostname suffix
	local zr_name=
	for dns_zone in $(getAuthoritativeForwardZones); do
		if grep -q -E "$dns_zone$" <<<"$hostname"; then
			zr_name=${hostname%.$dns_zone}
			break
		fi
	done
	[ -n $zr_name ] || {
		report_error "No authoritative zone found matching hostname \"$hostname\""
		return 1
	}

	record="$hostname. IN A $address"
	report_info "Adding record $record"
	ldapadd -Q <<-EOF
		dn: dlzHostName=$zr_name,dlzZoneName=$dns_zone,cn=forward-zones,ou=dns,$realm_ldap_base
		objectClass: dlzHost
		dlzHostName: $zr_name

		dn: dlzType=a,dlzHostName=$zr_name,dlzZoneName=$dns_zone,cn=forward-zones,ou=dns,$realm_ldap_base
		objectClass: dlzARecord
		dlzHostName: $zr_name
		dlzIPAddr: $address
		dlzRecordID: 0
		dlzTTL: 3600
		dlzType: a
	EOF
	[ $? -eq 0 ] || {
		report_error "Could not create record \"$record\" - LDAP add failed"
		return 1
	}
}

function createDNSCNAMERecord {
	local alias=$1
	local hostname=$2
	check_mandatory hostname alias || return 1

	requireAdmin
	# iterate through the list of valid forward zones
	# attempting to match the alias suffix
	local zr_name=
	for dns_zone in $(getAuthoritativeForwardZones); do
		if grep -q -E "$dns_zone$" <<<"$alias"; then
			zr_name=${alias%.$dns_zone}
			break
		fi
	done
	[ -n $zr_name ] || {
		report_error "No authoritative zone found matching hostname \"$alias\""
		return 1
	}
	# if the target of the alias is in the same zone use it's unqualified name
	local target="$hostname."
	target=${target%.$dns_zone.}

	record="$alias. IN CNAME $hostname."
	report_info "Adding record $record"
	ldapadd -Q <<-EOF
		dn: dlzHostName=$zr_name,dlzZoneName=$dns_zone,cn=forward-zones,ou=dns,$realm_ldap_base
		objectClass: dlzHost
		dlzHostName: $zr_name

		dn: dlzType=cname,dlzHostName=$zr_name,dlzZoneName=$dns_zone,cn=forward-zones,ou=dns,$realm_ldap_base
		objectClass: dlzCNAMERecord
		dlzHostName: $zr_name
		dlzData: $target
		dlzRecordID: 0
		dlzTTL: 3600
		dlzType: cname
	EOF
	[ $? -eq 0 ] || {
		report_error "Could not create record \"$record\" - LDAP add failed"
		return 1
	}
}

function getDHCPReservations {
	local name=$1
	check_mandatory name || return 1
	(
	set -o pipefail
	ldapsearch -LLLQ -s base -b cn=$name,cn=reservations,cn=config,ou=dhcp,$realm_ldap_base "(objectclass=dhcpHost)" dhcpStatements 2>/dev/null |\
		awk '
		/^dhcpStatements: / {
		    $1=""; $2=""
		    while ( NF > 0 ) {
		        gsub(" ","")
		        s = s$0
		        getline
		    }
		    gsub(","," ",s)
		    print s
		}
		'
	)
}

function createDHCPReservation {
	local name=$1
	check_mandatory name || return 1
	local fqhn=$(fullyQualifiedHostName $name)
	local r_name=${fqhn%.$root_domain}
	local u_name=${r_name%.*}
	local environment=${r_name#$u_name.}

	requireAdmin
	report_info "Adding DHCP reservation for \"$u_name\" to environment \"$environment\""

	local reservations
	reservations=$(getDHCPReservations $u_name) || {
		ldapadd -Q <<-EOF
			dn: cn=$u_name,cn=reservations,cn=config,ou=dhcp,$realm_ldap_base
			objectClass: dhcpHost
			objectClass: top
			objectClass: dhcpOptions
			cn: $u_name
			dhcpOption: dhcp-client-identifier "$u_name"
			dhcpStatements: fixed-address $fqhn
		EOF
		[ $? ] || {
			report_error "Could not create DHCP reservation for \"$u_name\" in \"$environment\" - LDAP add failed"
			return 1
		}
		return 0
	}

    for reservation in $reservations; do
		if [ "$reservation" == "$fqhn" ]; then
			report_info "A DHCP reservation already exists for \"$u_name\" to environment \"$environment\""
			return 0
		fi
	done

	local fixed_addresses=$(sed "s/ /,/g" <<< $reservations)
	ldapmodify -Q <<-EOF
		dn: cn=$u_name,cn=reservations,cn=config,ou=dhcp,$realm_ldap_base
		changetype: modify
		replace: dhcpStatements
		dhcpStatements: fixed-address $fixed_addresses,$fqhn
		-
	EOF
	[ $? ] || {
		report_error "Could not create DHCP reservation for \"$u_name\" in \"$environment\" - LDAP modify failed"
		return 1
	}
}

function fullyQualifiedDomainName {
	[ $# -le 1 ] || return 1
	local name=$1

	# for an unqualified hostname the domain returned will be that of the executing host
	# this is by design
	local hostname=${name%%.*}
	local domain=${name#$hostname};domain=${domain#.}
	: ${hostname:=$(hostname -s)}
	: ${domain:=$(hostname -d)}

	# if the constructed domain name doesn't include the root domain append it
	if (( $(expr match "$domain" ".*\.${root_domain//./\.}$") < 0 )); then
		domain=$domain.$root_domain
	fi
	echo $domain
}

function fullyQualifiedHostName {
	[ $# -le 1 ] || return 1
	local name=$1

	local hostname=${name%%.*}
	: ${hostname:=$(hostname -s)}
	echo $hostname.$(fullyQualifiedDomainName $name)
}

function realmRelativeHostName {
	[ $# -le 1 ] || return 1
	local name=$1

	# determine the realm-relative hostname to add to the LDAP store
	# site domains are relative to root_domain rather than realm_domain
	# so strip the longer of the two to obtain the realm-relative hostname
	# below are examples for the realm TEST.WOTIFGROUP.COM:
	#
	# <execution host>		  <supplied hostname>	 <site domain>		   <realm-relative hostname>
	# bar.test.wotifgroup.com   foo					 test.wotifgroup.com	 foo
	# bar.test.wotifgroup.com   foo.test				test.wotifgroup.com	 foo
	# bar.test.wotifgroup.com   foo.test.wotifgroup.com test.wotifgroup.com	 foo
	# bar.test.wotifgroup.com   foo.tst1				tst1.wotifgroup.com	 foo.tst1
	# bar.test.wotifgroup.com   foo.tst1.wotifgroup.com tst1.wotifgroup.com	 foo.tst1
	# zip.tst2.wotifgroup.com   baz					 tst2.wotifgroup.com	 baz.tst2
	# zip.tst2.wotifgroup.com   baz.test				test.wotifgroup.com	 baz
	# zip.tst2.wotifgroup.com   baz.test.wotifgroup.com test.wotifgroup.com	 baz
	# zip.tst2.wotifgroup.com   baz.tst2				tst2.wotifgroup.com	 baz.tst2
	# zip.tst2.wotifgroup.com   baz.tst2.wotifgroup.com tst2.wotifgroup.com	 baz.tst2

	local hostname=${name%%.*}
	: ${hostname:=$(hostname -s)}
	local domain=$(fullyQualifiedDomainName $name)
	local rdn=${domain%$realm_domain}
	rdn=${rdn%.$root_domain}
	echo -n $hostname${rdn:+.$rdn}
}

function getIPInEnvironment {
	local ip=$1
	check_mandatory ip || return 1
	local env_name=$2

	#env_name=$(getEnvironmentName $env_name)

	# do basic validation on the ip
	[ -n `expr "$ip" : '\(\([0-9]\{1,3\}\.\)\{1,3\}[0-9]\{1,3\}\)$'` ] || return 1
	local env_octet=$(( $(expr "$env_name" : '[^0-9]*\(.*\)') + 160 ))
	if [ -n "$env_name" ]; then
		echo -n 10.$env_octet.$(expr "$ip" : '.*\([0-9]\{1,3\}\.[0-9]\{1,3\}\)')
	else
		echo -n 10.$env_octet.$(expr "$ip" : '.*\.\([0-9]\{1,3\}\.[0-9]\{1,3\}\)')
	fi
}

function getEnvironmentName {
	[ $# -le 1 ] || return 1
	local hostname=$1
	expr "$(realmRelativeHostName $hostname)" : '.*\.\([^.]\+\)'
}

function createHostAccount {
	[ $# -le 1 ] || return 1
	name=$1
	rr_hostname=$(realmRelativeHostName $name)
	requireAdmin
	ldapadd -Q <<-EOF
		dn: host=$rr_hostname,ou=hosts,$realm_ldap_base
		objectClass: account
		objectClass: top
		objectClass: krbPrincipalAux
		uid: $rr_hostname
		host: $rr_hostname
		krbCanonicalName: host/$(fullyQualifiedHostName $name)@$realm
	EOF

	# only proceed if the ldapadd succeeded
	[ $? -eq 0 ] || {
		report_error "Could not create host account \"$rr_hostname\" - LDAP add failed"
		return 1
	}

	$kadmin -c $KRB5CCNAME -q "ank -x dn=\"host=$rr_hostname,ou=hosts,$realm_ldap_base\" -policy host -randkey host/$(fullyQualifiedHostName $name)" 2>&1 > /dev/null || {
		report_error "Could not create host account \"$rr_hostname\" - Kerberos addprincipal failed"
		return 1
	}

	ldapmodify -Q <<-EOF
		dn: host=$rr_hostname,ou=hosts,$realm_ldap_base
		changetype: modify
		add: krbPrincipalName
		krbPrincipalName: host/$rr_hostname@$realm
		-
		add: krbPrincipalAliases
		krbPrincipalAliases: host/$rr_hostname@$realm
		-
	EOF
}

function extractKeytab {
	# Parse arguments
	args=$(getopt -o k: -- "$@") || return 1
	eval set -- "$args"
	while true; do
		case "$1" in
			-k) shift; keytab="$1"; shift;;
			--) shift; break;;
			*) report_error "Internal error!"; return 1 ;;
		esac
	done
	[ $# -eq 1 ] || return 1
	princ=$1
	check_mandatory keytab princ || return 1

	requireAdmin
	$kadmin -c $KRB5CCNAME -q "ktadd -k $keytab $princ" 2>&1 > /dev/null || {
		report_error "Could not extract keytab for \"$princ\" - ktadd failed"
		return 1
	}
}

