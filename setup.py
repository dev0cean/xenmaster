#!/usr/bin/env python
from distutils import log
from distutils.dep_util import newer
from setuptools.command.install import install as _install
from setuptools import setup, find_packages, Command
import os
from stat import ST_MODE
import sys


if sys.platform == 'darwin' and [int(x) for x in os.uname()[2].split('.')] >= [11, 0, 0]:
    # clang is serious now
    extra_compile_args = ['-Werror', '-Wunused-command-line-argument-hard-error-in-future', '-Qunused-arguments']
else:
    extra_compile_args = ['-Werror']


class install(_install):
    # ensure that if this is installed "old style"
    # install_config.run() will still get invoked
    sub_commands = _install.sub_commands + [
        ('install_config', lambda self:True)]

    def do_egg_install(self):
        """
        filthy hack for setuptools authors lack of OO fundamentals
        """
        self.run_command("install_config")
        _install.do_egg_install(self)


class install_config(Command):
    description = "install config files"

    user_options = [
        ("config-files=",
         None,
         "dictionary mapping src config file locations (relative to setup.py) "
         "to destination locations (relative to the config_prefix)"),
    ]

    def initialize_options(self):
        self.root = None
        self.force = None
        self.prefix = None
        self.config_files = None
        self.outfiles = []

    def finalize_options(self):
        self.set_undefined_options(
            'install',
            ('root', 'root'),
            ('force', 'force'),
            ('prefix', 'prefix'))

    def run(self):
        if self.config_files is None:
            return
        config_prefix = "/etc"
        if self.prefix and self.prefix != "/usr":
            config_prefix = self.prefix + "/etc"
        config_prefix = (self.root or "") + os.path.normcase(
            os.path.normpath(config_prefix))
        for src, dest in self.config_files.iteritems():
            dest = os.path.normpath(os.path.expanduser(dest))
            if not os.path.isabs(dest):
                dest = os.path.join(config_prefix, dest)

            if not self.force and not newer(src, dest):
                log.debug("not copying %s (up-to-date)", dest)
                continue

            self.mkpath(os.path.dirname(dest))
            if os.path.isdir(src):
                self.copy_tree(src, dest)
            else:
                self.copy_file(src, dest)
            self.outfiles.append(dest)
            if os.name == 'posix':
                if self.dry_run:
                    log.info("changing mode of %s", dest)
                else:
                    mode = ((os.stat(dest)[ST_MODE]) | 0660) & 07777
                    log.info("changing mode of %s to %o", dest, mode)
                    os.chmod(dest, mode)

    def get_outputs(self):
        return self.outfiles


setup(
    name="xenmaster",
    version="2.2.0",
    description="",
    package_dir={'': "src/main/python"},
    packages=find_packages("src/main/python"),
    package_data={'': ['*.conf']},
    scripts=[
        "src/main/python/xm",
    ],
    dependency_links=[
        "https://bitbucket.org/dwhitla/python-ocsutil/get/1.0.4.zip#egg=ocsutil-1.0.4",
        "git+https://bitbucket.org/dev0cean/libxm@56c0a86#egg=libxm-1.2.0",
        "https://bitbucket.org/dev0cean/libxm-lvm2-storage/get/1.2.0.zip#egg=libxm-lvm2-storage-1.2.0",
        "https://dev.mysql.com/get/Downloads/Connector-Python/mysql-connector-python-2.1.3.zip"
        "#egg=mysql-connector-python-2.1.3"
    ],
    install_requires=[
        "ocsutil>=1.0.4",
        "libxm>=1.2.0",
        "libxm-lvm2-storage>=1.2.0",
        "python-ldap",
        #"python-ldap==2.4.10",
        "mysql-connector-python==2.1.3",
    ],
    url='',
    license='',
    author="dwhitla",
    author_email="dave.whitla@devocean.net",
    classifiers=[
        "Programming Language :: Python :: 2",
        "Development Status :: 3 - Alpha",
        "Topic :: System :: Systems Administration :: Virtualisation"
    ],
    cmdclass={
        "install": install,
        "install_config": install_config,
    },
    options={"install_config": {"config_files": {
        "src/main/resources/cron/xenmaster": "xenmaster/crontab",
        "src/main/resources/etc/backup-db.sh": "xenmaster/backup-db.sh",
        "src/main/resources/etc/xm.conf": "xenmaster/xm.conf",
    }}},
)
